//  JQUERY FUNCTIONS
$(document).ready(function () {

    $('.logout').on('click', function (e) {
        e.preventDefault();
        swal({
            title               : "Deseja fazer logout ?",
            type                : "question",
            confirmButtonColor  : "red",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                document.getElementById('logout-form').submit();
            }
        }.bind(this));
    });

    $('.delete').on('click', function (e) {
        e.preventDefault();

        let url = $(this).attr('data-action')
        let redirect = $(this).attr('data-redirect')

        swal({
            title               : "Deseja realmente deletar ?",
            type                : "question",
            confirmButtonColor  : "red",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: "JSON",
                    success: function (response)
                    {
                        swal({
                            title   : response.data.message,
                            text    : response.data.info,
                            type    : response.data.status,
                            timer   : 2000,
                            showConfirmButton: false
                        },setTimeout(function() {
                            window.location.href = redirect;
                        }, 2000))
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                        swal({
                            title   : "Occoreu um erro ao deletar",
                            // text    : xhr.responseText,
                            type    : "error",
                            timer   : 2000,
                            showConfirmButton: false
                        });
                    }
                    });

            }
        }.bind(this));
    });

    $('.not-active').prop("disabled", true);


    $('.load_report').on('click', function (e) {
        e.preventDefault();

        let url = $(this).attr('data-action');
        let code = $("#code").val();

        let iframe = "<iframe src='file.pdf' width='800' height='800' class='col-lg-10 col-md-10 col-sm-10 col-xs-10'></iframe>";
        let load_image = "<img src='/img/loading.gif' alt='loading...' " +
            "style='display: block;\n" +
            "  margin-left: auto;\n" +
            "  margin-right: auto;\n" +
            "  width: 50%;' />";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#card").html(load_image);

        setTimeout(() => {
            $.ajax({
                type: "POST",
                url: url,
                data: {code: code},
                cache: false,
                success: function(response)
                {
                    $("#card").html(iframe);

                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                    swal({
                        title   : "Occoreu um erro",
                        text    : xhr.responseText.message,
                        type    : "error",
                        timer   : 2000,
                        showConfirmButton: false
                    });
                }
            });
        }, 3000);

    });


    $("#input-async").fileinput({
        uploadUrl: "/brazil/admin/csv/import",
        uploadExtraData:{'_token':$('meta[name="csrf-token"]').attr('content')},
        allowedFileExtensions: ['csv'],
        maxFileCount: 1,
        showBrowse: false,
        browseOnZoneClick: true
    }).on('fileuploaded', function(event, data) {
        swal({
            title   : "OK",
            text    : "Arquivo processado com sucesso",
            type    : "success",
            timer   : 2000,
            showConfirmButton: false
        },setTimeout(function() {
            window.location.href = "/brazil/admin/csv/import";
        }, 2000))
    });

});
