<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExportInformProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:export-products-to-csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $select =
        "SELECT
           DISTINCT CADMATERIAL.CODIGOAUXILIAR as SKU,
		   /*CADMATERIAL.APLICACAO as REFERENCIA,*/
		   CADMATERIAL.DESCRICAO as Nome,
		   (SELECT TOP 1 CADPARCEIRO.DESCRICAO FROM CADPARCEIRO LEFT JOIN MATERIALPARCEIRO ON CADMATERIAL.CODIGOMATERIAL = MATERIALPARCEIRO.CODIGOMATERIAL WHERE MATERIALPARCEIRO.CODIGOPARCEIRO = CADPARCEIRO.CODIGOPARCEIRO) AS Marca,
		   CADMATERIAL.CODIGOTABELA6 AS Categoria,
		   CADTABELA6.DESCRICAO AS CategoriaNome,
		   (SELECT TOP 1 MATERIALCODIGOSBARRA.CODIGOBARRA FROM MATERIALCODIGOSBARRA WHERE CADMATERIAL.CODIGOMATERIAL = MATERIALCODIGOSBARRA.CODIGOMATERIAL) AS Codebar,
		   (SELECT A.PRECOVENDA1 FROM TABELAPRECOITEM A INNER JOIN CADMATERIAL B ON A.CODIGOTABELAPRECOITEM = B.CODIGOTABELAPRECOITEM WHERE A.CODIGOTABELAPRECO='01001' AND B.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL) AS Preço,
		   CADMATERIAL.UNIDADESAIDA,
		   (SELECT MAX(A.DISPONIVEL1/C.FATOR) FROM MATERIALALMOXARIFADO A INNER JOIN CADMATERIAL B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL INNER JOIN CADUNIDADE C ON B.UNIDADESAIDA = C.NOMENCLATURA WHERE A.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL AND A.CODIGOALMOXARIFADO = MATERIALALMOXARIFADO.CODIGOALMOXARIFADO) AS Estoque,
		   CADMATERIAL.CODIGONCM,
		   CADMATERIAL.DATACADASTRO,
		   CADMATERIAL.CODIGOMATERIAL,
		   CADMATERIAL.CODIGOIMAGEM
		FROM
		   CADMATERIAL
		      LEFT OUTER JOIN MATERIALALMOXARIFADO ON CADMATERIAL.CODIGOMATERIAL = MATERIALALMOXARIFADO.CODIGOMATERIAL
		      LEFT OUTER JOIN CADTIPOTRIBUTACAO ON CADMATERIAL.CODIGOTIPOTRIBUTACAO = CADTIPOTRIBUTACAO.CODIGOTIPOTRIBUTACAO
		      LEFT JOIN CADTABELA6 ON CADMATERIAL.CODIGOTABELA6 = CADTABELA6.CODIGOTABELA6
		      LEFT OUTER JOIN CADIMAGEM ON CADIMAGEM.CODIGOIMAGEM = CADMATERIAL.CODIGOIMAGEM
		WHERE
		   CadMaterial.Status = 'A'									/*  ESTÁ ATIVO  */
		   AND MaterialAlmoxarifado.CodigoAlmoxarifado = '00001' 	/*  ALMOXARIFADO  */
		   /* AND CadParceiro.Descricao  like '%compactor%'			  FORNECEDOR  */
		   AND (SELECT A.PRECOVENDA1 FROM TABELAPRECOITEM A INNER JOIN CADMATERIAL B ON A.CODIGOTABELAPRECOITEM = B.CODIGOTABELAPRECOITEM WHERE A.CODIGOTABELAPRECO='01001' AND B.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL) > 0 /*  PREÇO MAIOR QUE 0  */
		   AND cadmaterial.CODIGOIMAGEM IS NOT NULL					/*  TEM IMAGEM NO CATÁLOGO  */
		   AND cadmaterial.CODIGOTABELA6 = 00006					/*CODIGO NO CATÁLOGO (numero ou is not null) */
		   AND (SELECT MAX(A.DISPONIVEL1/C.FATOR) FROM MATERIALALMOXARIFADO A INNER JOIN CADMATERIAL B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL INNER JOIN CADUNIDADE C ON B.UNIDADESAIDA = C.NOMENCLATURA WHERE A.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL AND A.CODIGOALMOXARIFADO = MATERIALALMOXARIFADO.CODIGOALMOXARIFADO) > 0 /*  SE TEM QUANTIDADE MAIOR QUE 0  */
		ORDER BY
		   Nome ";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $data = DB::connection('sqlsrv')->select($this->select);
        $this->data_to_csv($data, 'public/produtos.csv');

        $this->info('Ordenação finalizada com sucesso!');
    }

    function data_to_csv($data, $filename): void
    {
        $file = fopen($filename, 'w');
        $header = ['Identificador URL', 'Nome', 'Categorias', "Preço", "Preço promocional", 'Peso','Altura', 'Largura', 'Comprimento', 'Estoque', 'SKU', "Código de barras", 'Exibir na loja', 'Frete gratis',	"Descrição", 'Tags', "Título para SEO", "Descrição para SEO", 'Marca'];

        fputcsv($file, $header);

        foreach ($data as $row) {
            $arrayRow = collect($row)->toArray();
            $arrayRow['Estoque'] > 50 ? $show = 'SIM': $show = 'NÃO';

            $outputRow = [
                'Identificador URL' => str_replace(" ", "", $arrayRow['SKU']),
                'Nome'              => str_replace("*", "", $arrayRow['Nome']),
                'Categorias'        => $this->getCategorie($arrayRow['CategoriaNome']),
                "Preco"             => $arrayRow['Preço'],
                "Preco promocional" => null,
                'Peso'              => null,
                'Altura'            => null,
                'Largura'           => null,
                'Comprimento'       => null,
                'Estoque'           => $arrayRow['Estoque'],
                'SKU'               => $arrayRow['SKU'],
                "Codigo de barras"  => $arrayRow['Codebar'],
                'Exibir na loja'    => $show,
                'Frete gratis'      => 'NÃO',
                "Descricao"         => null,
                'Tags'              => null,
                "Titulo para SEO"   => str_replace("*", "", $arrayRow['Nome']),
                "Descricao para SEO"=> "Compre ".str_replace("*", "", $arrayRow['Nome']).". Faça seu pedido online, pague-o e receba em casa!",
                'Marca'             => $arrayRow['Marca']
            ];

            fputcsv($file, $outputRow);
        }

        fclose($file);
    }

    function getCategorie($name): string
    {
        if($name == 'PAPELARIA' || $name == 'MOCHILAS' || $name == 'MOCHILAS 2' || $name == 'PAPELARIA - GERAL' || $name == 'PAPELARIA - CADERNOS'|| $name == 'MATERIAL DE EXPEDIENTE'){
            return 'Papelaria';
        }
        elseif ($name == 'BRINQUEDO IMPORTADO'){
            return 'Brinquedos';
        }
        elseif ($name == 'VIDRO' || $name == 'UTILIDADE IMPORTADA' || $name == 'UTILIDADE NACIONAL'){
            return "Utilidade Doméstica";
        }
        elseif ($name == 'PRESENTES'){
            return 'Presentes e Decoração';
        }
        else{
            return 'Diversos';
        }
    }
}
