<?php

namespace App\Console\Commands;

use App\Entities\Files;
use Illuminate\Console\Command;
use \Illuminate\Support\Facades\Storage;

class ReadFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'read:files {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read XML Files in Storage folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $type = $this->argument('type');

        if($type == 'XML'){
            self::xmls();
        }

        if($type == 'sped'){
            self::sped();
        }
    }

    public function sped(): void
    {
        $this->info('Comando iniciado');
        $this->info('Lendo Arquivos');

        $flag = false;

        $text = '';

        foreach (Storage::disk('public')->files('TXT') as $filePath) {
            $output = [];

            $file = Storage::disk('public')->get($filePath);

            $array = explode("\n", $file);

            foreach ($array ?? [] as $index => $line) {
                $arrayLine = explode("|", $line);

                if (($arrayLine[1] ?? '') == 'C100' && $arrayLine[5] == '65') {
                    $flag = true;
                }

                if (($arrayLine[1] ?? '') == 'C100' && $arrayLine[5] == '55') {
                    $flag = false;
                }

                if (($arrayLine[1] ?? '') == 'C100' && $arrayLine[5] == '55') {
                    $flag = false;
                }

                if (($arrayLine[1] ?? '') == 'C100' && $arrayLine[2] == '0') {
                    $flag = false;
                }

                if($flag && (($arrayLine[1] ?? '') == 'C100' || ($arrayLine[1] ?? '') == 'C190')){
                    $output[$index] = $arrayLine;
                }

            }

            foreach($output as $line){

                foreach ($line as $key => $word){
                    if(($key != 0)){
                        $text .= '|'.$word;
                    }
                }

                $text .="\n";
            }

            Storage::disk('public')->put("OUTPUT/".$filePath, $text);
        }
    }


    public function xmls(): void
    {
        $this->info('Comando iniciado');
        $this->info('Limpando tabela de arquivos');
        Files::truncate();
        $this->info('Cadastrando novos arquivos');

        foreach (Storage::disk('public')->directories('XML') as $directorie) {
            $filesnames = Storage::disk('public')->allFiles(''.$directorie);
            $bar = $this->output->createProgressBar(count($filesnames));
            $bar->start();

            foreach($filesnames as $filename){
                $arrayFiels = explode("/", $filename);

                Files::create([
                    'path' => $filename,
                    'mime' => 'text/xml',
                    'extra_field_1' => $arrayFiels[2],
                    'extra_field_2' => $arrayFiels[3],
                ]);

                $bar->advance();
            };

            $bar->finish();
        }

        $this->info('Fim');
    }
}
