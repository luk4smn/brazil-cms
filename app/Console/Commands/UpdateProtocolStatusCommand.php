<?php


namespace App\Console\Commands;


use App\Entities\Protocol;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateProtocolStatusCommand extends Command
{
    protected $signature = 'command:update-protocol-status';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        Log::info("Comando update-protocol-status iniciado");

        $protocols = Protocol::whereDate('created_at', '<', now()->subWeeks(1))
            ->whereIn('status', [Protocol::STATUS_PENDING, Protocol::STATUS_IN_PROGRESS])
            ->each(function ($item){
                $item->status = Protocol::STATUS_COMPLETED;
                $item->save();
            });
    }

}
