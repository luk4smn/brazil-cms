<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderProductsLocals extends Command
{
    protected $signature = 'command:order-products-locals';
    protected $description = 'Ordena nome dos locais de materiais cadastrados no ARGOS';

    // CAMPO CG
    protected $select1 =
        "SELECT CODIGOMATERIAL, CAMPOLIVREA1
         FROM [BRAZIL_INFORM].[DBO].CADMATERIAL
         WHERE CAMPOLIVREA1 IS NOT NULL AND CAMPOLIVREA1 <> ''";

    // CAMPO JP
    protected $select2 =
        "SELECT CODIGOMATERIAL, CAMPOLIVREA2
         FROM [BRAZIL_INFORM].[DBO].CADMATERIAL
         WHERE CAMPOLIVREA2 IS NOT NULL AND CAMPOLIVREA2 <> ''";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        Log::info("Comando order-products-locals iniciado");

        $this->info('Iniciando a ordenação dos locais de materiais...');

        DB::connection('sqlsrv')->transaction(function () {
            $this->info('Processando CAMPOLIVREA1...');
            $this->order($this->select1, 'CAMPOLIVREA1');
            $this->info('Processando CAMPOLIVREA2...');
            $this->order($this->select2, 'CAMPOLIVREA2');
        });

        $this->info('Ordenação finalizada com sucesso!');
    }

    public function order($select, $field): void
    {
        $this->info("Selecionando dados de $field...");
        $data = DB::connection('sqlsrv')->select($select);

        $this->info("Processando dados de $field...");
        $data = collect($data)
            ->map(function ($item) use ($field) {
                $locals = preg_replace('/\s+/', '', $item->$field);         // Remove espaços
                $locals = preg_replace("/[\r\n]/", '', $locals);            // Remove quebras de linha
                $locals = explode(",", $locals);
                $locals = array_filter(array_unique($locals), function ($local) {           // Remove duplicados, valores inválidos
                    return !empty(trim($local));
                });
                asort($locals);                                                     // Ordena os locais

                return [
                    'CODIGOMATERIAL' => $item->CODIGOMATERIAL,
                    $field => implode(", ", $locals)
                ];
            })
            ->filter(function ($item) use ($field) {
                // Verifica se a string não está vazia
                return !empty($item[$field]);
            });

        $this->info("Atualizando o banco de dados para $field...");
        foreach ($data as $item) {
            $this->info("Atualizando CODIGOMATERIAL: {$item['CODIGOMATERIAL']} com $field: {$item[$field]}");
            DB::connection('sqlsrv')->update(
                "UPDATE [BRAZIL_INFORM].[DBO].CADMATERIAL SET $field = ? WHERE CODIGOMATERIAL = ?",
                [$item[$field], $item['CODIGOMATERIAL']]
            );
        }

        $this->info("Atualizações finalizadas para $field.");
    }

}


