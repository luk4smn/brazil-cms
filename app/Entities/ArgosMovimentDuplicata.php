<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArgosMovimentDuplicata extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'MOVIMENTODUPLICATA';

     public $timestamps = false;

    protected $fillable = [
        "ORDEMMOVIMENTO",
        "DOCUMENTO",
        "ORDEM",
        "DATAEMISSAO",
        "DATAVENCIMENTO",
        "VALORDUPLICATA",
        "IMPRESSO",
        "ORIGEMATUALIZOUCPR",
        "ORIGEMATUALIZOUCC",
        "ORDEMITEM",
        'CODIGOTIPOEVENTO',

    ];

    public function moviment(): BelongsTo
    {
        return $this->belongsTo(ArgosMoviment::class, 'ordemmovimento', 'ordemmovimento');
    }


}
