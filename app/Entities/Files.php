<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;


class Files extends Model
{
    protected $table = 'files';

    protected $fillable = [
        'path',
        'mime',
        'extra_field_1',
        'extra_field_2',
        'extra_field_3',
    ];


   public function scopeCountXML()
   {
   		set_time_limit(0);
   		return self::where('extra_field_2', $this->attributes['extra_field_2'])->count();
   }

   public function getYearXML(): string
   {
   		$arrayFiels = explode("/", $this->attributes['path']);
   		return $arrayFiels[1];
   }

}
