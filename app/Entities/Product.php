<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'product';

    protected $fillable = [
        'cod_mat',
        'cod_aux',
        'codebar',
        'descricao',
        'unidade_entrada',
        'unidade_saida',
        'fator_saida',
        'referencia',
        'cod_fornecedor',
        'nome_fornecedor',
        'estoque_total',
        'estoque_matriz',
        'estoque_inartel',
        'estoque_bayeux',
        'estoque_loja_avaria',
        'preco_venda',
        'user_field1',
        'user_field2',
        'user_field3',
        'cost'
    ];

    const CODIGOALMOXARIFADO = [
        'matriz'    => '(00001, 00002, 00003, 00004, 00005)',
        'bayeux'    => '(00011, 00012)',
        'jp'        => '(10001, 10010, 10019)',
    ];

    public function getCostAttribute(){
        $codAlmox = Product::CODIGOALMOXARIFADO['matriz'];

        $sql = "SELECT top 1 ULTIMOCUSTO
	        FROM Brazil_Inform.dbo.MATERIALALMOXARIFADO A
	        INNER JOIN Brazil_Inform.dbo.CADMATERIAL B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL
	        WHERE CODIGOALMOXARIFADO IN {$codAlmox}
	        AND B.CODIGOMATERIAL = '{$this->cod_mat}'";

        $data = \DB::connection('sqlsrv2')->select($sql);

        return collect($data)->first()->ULTIMOCUSTO ?? 0.00;
    }

}
