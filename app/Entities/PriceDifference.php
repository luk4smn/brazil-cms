<?php


namespace App\Entities;


use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PriceDifference extends Entity
{
    protected $table = 'price_differences';

    protected $fillable = [
        'provider',
        'notes',
        'status',
        'invoice',
        'user_id',
    ];

    const
        STATUS_PENDING     = 1,
        STATUS_IN_PROGRESS = 2,
        STATUS_RESOLVED    = 3;

    protected static $status = [
        self::STATUS_PENDING     => 'Pendente',
        self::STATUS_IN_PROGRESS => 'Procedendo',
        self::STATUS_RESOLVED    => 'Resolvido',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(PriceDifferenceItem::class, 'price_difference_id');
    }


    public static function getStatus(): array
    {
        return self::$status;
    }

    public function getStatusName()
    {
        if($this->attributes['status'])
            return self::$status[$this->attributes['status']];
    }

    public static function getAmount($id)
    {
        $difference = PriceDifference::findOrFail($id);

        return $difference
            ->items
            ->sum('amount');
    }

    public static function getAmountWithTax($id)
    {
        $difference = PriceDifference::findOrFail($id);

        return $difference
            ->items
            ->sum('amount_with_tax');
    }

}
