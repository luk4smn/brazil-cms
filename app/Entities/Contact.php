<?php


namespace App\Entities;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Contact extends Entity
{
    protected $table = 'contacts';

    const
        COLABORADOR         = 1,
        FORNECEDOR          = 2,
        REPRESENTANTE       = 3,
        PROVEDOR_DE_SERVICO = 4,
        SUPORTE             = 5,
        EMPRESA_GRUPO       = 6;

    protected static $types = [
        self::COLABORADOR         => 'Colaborador',
        self::EMPRESA_GRUPO       => 'Empresa do Grupo',
        self::FORNECEDOR          => 'Fornecedor',
        self::PROVEDOR_DE_SERVICO => 'Provedor de Serviços',
        self::REPRESENTANTE       => 'Representante',
        self::SUPORTE             => 'Suporte',
    ];

    protected $fillable = [
        'name',
        'email',
        'type',
        'address',
        'notes',
        'telephone1',
        'telephone2',
        'telephone3',
        'cellphone1',
        'cellphone2',
        'cellphone3',
    ];

    public function getTypeName()
    {
       if($this->attributes['type'])
           return self::$types[$this->attributes['type']];
    }

    public static function getTypes(): array
    {
        return self::$types;
    }

}
