<?php


namespace App\Entities;


use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ProductSelect
{
    public static function getQuery($stringCategorie): string
    {
        return
            "SELECT
           DISTINCT CADMATERIAL.CODIGOAUXILIAR as SKU,
		   /*CADMATERIAL.APLICACAO as REFERENCIA,*/
		   CADMATERIAL.DESCRICAO as Nome,
		   (SELECT TOP 1 CADPARCEIRO.DESCRICAO FROM CADPARCEIRO LEFT JOIN MATERIALPARCEIRO ON CADMATERIAL.CODIGOMATERIAL = MATERIALPARCEIRO.CODIGOMATERIAL WHERE MATERIALPARCEIRO.CODIGOPARCEIRO = CADPARCEIRO.CODIGOPARCEIRO) AS Marca,
		   CADMATERIAL.CODIGOTABELA6 AS Categoria,
		   CADTABELA6.DESCRICAO AS CategoriaNome,
		   (SELECT TOP 1 MATERIALCODIGOSBARRA.CODIGOBARRA FROM MATERIALCODIGOSBARRA WHERE CADMATERIAL.CODIGOMATERIAL = MATERIALCODIGOSBARRA.CODIGOMATERIAL) AS Codebar,
		   (SELECT A.PRECOVENDA1 FROM TABELAPRECOITEM A INNER JOIN CADMATERIAL B ON A.CODIGOTABELAPRECOITEM = B.CODIGOTABELAPRECOITEM WHERE A.CODIGOTABELAPRECO='01001' AND B.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL) AS Preço,
		   CADMATERIAL.UNIDADESAIDA,
		   (SELECT MAX(A.DISPONIVEL1/C.FATOR) FROM MATERIALALMOXARIFADO A INNER JOIN CADMATERIAL B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL INNER JOIN CADUNIDADE C ON B.UNIDADESAIDA = C.NOMENCLATURA WHERE A.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL AND A.CODIGOALMOXARIFADO = MATERIALALMOXARIFADO.CODIGOALMOXARIFADO) AS Estoque,
		   CADMATERIAL.CODIGONCM,
		   CADMATERIAL.DATACADASTRO,
		   CADMATERIAL.CODIGOMATERIAL,
		   CADMATERIAL.CODIGOIMAGEM
		FROM
		   CADMATERIAL
		      LEFT OUTER JOIN MATERIALALMOXARIFADO ON CADMATERIAL.CODIGOMATERIAL = MATERIALALMOXARIFADO.CODIGOMATERIAL
		      LEFT OUTER JOIN CADTIPOTRIBUTACAO ON CADMATERIAL.CODIGOTIPOTRIBUTACAO = CADTIPOTRIBUTACAO.CODIGOTIPOTRIBUTACAO
		      LEFT JOIN CADTABELA6 ON CADMATERIAL.CODIGOTABELA6 = CADTABELA6.CODIGOTABELA6
		      LEFT OUTER JOIN CADIMAGEM ON CADIMAGEM.CODIGOIMAGEM = CADMATERIAL.CODIGOIMAGEM
		WHERE
		   (
           CadMaterial.Status = 'A'									/*  ESTÁ ATIVO  */
		   AND MaterialAlmoxarifado.CodigoAlmoxarifado = '00001' 	/*  ALMOXARIFADO  */
		   /* AND CadParceiro.Descricao  like '%compactor%'			  FORNECEDOR  */
		   AND (SELECT A.PRECOVENDA1 FROM TABELAPRECOITEM A INNER JOIN CADMATERIAL B ON A.CODIGOTABELAPRECOITEM = B.CODIGOTABELAPRECOITEM WHERE A.CODIGOTABELAPRECO='01001' AND B.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL) >= 0 /*  PREÇO MAIOR QUE 0  */
		   AND cadmaterial.CODIGOIMAGEM IS NOT NULL					/*  TEM IMAGEM NO CATÁLOGO  */
		   AND (SELECT MAX(A.DISPONIVEL1/C.FATOR) FROM MATERIALALMOXARIFADO A INNER JOIN CADMATERIAL B ON A.CODIGOMATERIAL = B.CODIGOMATERIAL INNER JOIN CADUNIDADE C ON B.UNIDADESAIDA = C.NOMENCLATURA WHERE A.CODIGOMATERIAL = CADMATERIAL.CODIGOMATERIAL AND A.CODIGOALMOXARIFADO = MATERIALALMOXARIFADO.CODIGOALMOXARIFADO) > 0 /*  SE TEM QUANTIDADE MAIOR QUE 0  */".$stringCategorie."
           )
		ORDER BY
		   Nome";
    }


    public static function data_to_csv($data, $cut, $filename): BinaryFileResponse
    {

        $file = fopen($filename, 'w');
        $header = ['Identificador URL', 'Nome', 'Categorias', "Preço", "Preço promocional", 'Peso','Altura', 'Largura', 'Comprimento', 'Estoque', 'SKU', "Código de barras", 'Exibir na loja', 'Frete gratis',	"Descrição", 'Tags', "Título para SEO", "Descrição para SEO", 'Marca'];

        fputcsv($file, $header);

        $data = collect($data)->unique('SKU');

        foreach ($data as $row) {
            $arrayRow = collect($row)->toArray();
            $arrayRow['Estoque'] > $cut ? $show = 'SIM': $show = 'NÃO';

            if(($arrayRow['CODIGOIMAGEM'] ?? false) && ($arrayRow['Preço'] > 0 ?? false) && ($arrayRow['Estoque'] >= 0 ?? false)){
                $outputRow = [
                    'Identificador URL' => str_replace(" ", "", $arrayRow['SKU']),
                    'Nome'              => self::getName(str_replace("*", "", $arrayRow['Nome'])),
                    'Categorias'        => self::getCategorie($arrayRow['CategoriaNome']),
                    "Preco"             => $arrayRow['Preço'],
                    "Preco promocional" => null,
                    'Peso'              => null,
                    'Altura'            => null,
                    'Largura'           => null,
                    'Comprimento'       => null,
                    'Estoque'           => $arrayRow['Estoque'],
                    'SKU'               => $arrayRow['SKU'],
                    "Codigo de barras"  => $arrayRow['Codebar'],
                    'Exibir na loja'    => $show,
                    'Frete gratis'      => 'NÃO',
                    "Descricao"         => null,
                    'Tags'              => null,
                    "Titulo para SEO"   => str_replace("*", "", $arrayRow['Nome']),
                    "Descricao para SEO"=> "Compre ".str_replace("*", "", $arrayRow['Nome']).". Faça seu pedido online, pague-o e receba em casa!",
                    'Marca'             => $arrayRow['Marca']
                ];

                fputcsv($file, $outputRow);
            }
        }

        fclose($file);

        return response()->download($filename);
    }

    public static function getCategorie($name): string
    {
        if($name == 'PAPELARIA' || $name == 'MOCHILAS' || $name == 'MOCHILAS 2' || $name == 'PAPELARIA - GERAL' || $name == 'PAPELARIA - CADERNOS'|| $name == 'MATERIAL DE EXPEDIENTE'){
            return 'Papelaria';
        }
        elseif ($name == 'BRINQUEDO IMPORTADO'){
            return 'Brinquedos';
        }
        elseif ($name == 'VIDRO' || $name == 'UTILIDADE IMPORTADA' || $name == 'UTILIDADE NACIONAL'){
            return "Utilidade Doméstica";
        }
        elseif ($name == 'PRESENTES'){
            return 'Presentes e Decoração';
        }
        else{
            return 'Diversos';
        }
    }

    public static function getName($string)
    {

        $words = [
            'APAG'      => 'APAGADOR',
            'APONT'     => 'APONTADOR',
            'ARQ'       => 'ARQUIVO',
            'LAP'       => 'LAPISEIRA',
            'MOCH'      => 'MOCHILA',
            'CAN'       => 'CANETA',
            'HIDROG'    => 'HIDROGRAFICA',
            'ELAST'     => 'ELASTICO',
            'CORRET'    => 'CORRETIVO',
            'BORR'      => 'BORRACHA',
            'LANCH'     => 'LANCHEIRA',
            'PREND'     => 'PRENDEDOR',
            'ALMOF'     => 'ALMOFADA',
            'EST'       => 'ESTOJO',
            'ESC'       => 'ESCOLAR',
            'MARC'      => 'MARCADOR',
            'CAD'       => 'CADERNO'
        ];

        foreach ($words as $key => $value) {
            if(is_int(strpos($string, $key.' '))){
                $string = str_replace($key, $value, $string);
            }
        }

        return $string;
    }

}
