<?php


namespace App\Entities;


use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use phpDocumentor\Reflection\Types\Self_;

class Protocol extends Entity{

    protected $table = 'protocols';

    protected $fillable = [
        'recipient',
        'type',
        'notes',
        'status',
        'user_id',
    ];

    const
        STATUS_PENDING     = 1,
        STATUS_IN_PROGRESS = 2,
        STATUS_COMPLETED   = 3;

    const
        TYPE_EQUIPMENT     = 1,
        TYPE_PRODUCT       = 2;

    protected static $status = [
        self::STATUS_PENDING     => 'Pendente',
        self::STATUS_IN_PROGRESS => 'Em andamento',
        self::STATUS_COMPLETED   => 'Concluído'
    ];

    protected static $types = [
        self::TYPE_EQUIPMENT     => 'Equipamento',
        self::TYPE_PRODUCT       => 'Mercadoria',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(ProtocolItems::class, 'protocol_id');
    }

    public static function getStatus(): array
    {
        return self::$status;
    }

    public function getTypeName()
    {
        if($this->attributes['type'])
            return self::$types[$this->attributes['type']];
    }

    public function getStatusName()
    {
        if($this->attributes['status'])
            return self::$status[$this->attributes['status']];
    }

    public function scopeSearch(Builder $builder, $query,  $params = []): Builder
    {
        if(!empty($query)){
            $builder->where(function(Builder $builder) use($query, $params) {
                return $builder
                    ->where($params[0], $query)
                    ->orWhere($params[1] ?? '', 'like', "%$query%")
                    ->orWhere($params[2] ?? '', 'like', "%$query%")
                    ->orWhere($params[3] ?? '', 'like', "%$query%");
            });
        }

        return $builder;
    }

    public function getItemsData()
    {
        if($this->type = self::TYPE_PRODUCT){
            $names = $this->items->map(function ($item, $key){
                return $item->provider;
            });

            $names->unique()->map(function ($item, $key){
                if($key > 0){
                    $this->output .= ' - '.$item;
                }else{
                    $this->output .= $item;
                }
            });
        }

        return $this->output;
    }

}
