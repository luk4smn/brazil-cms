<?php


namespace App\Entities;


class PriceDifferenceItem extends Entity
{
    protected $table = 'price_differences_items';

    protected $fillable = [
        'aux_code',
        'ref',
        'description',
        'quantity',
        'original_price',
        'recived_price',
        'tax',
        'amount',
        'amount_with_tax',
        'price_difference_id'

    ];

}
