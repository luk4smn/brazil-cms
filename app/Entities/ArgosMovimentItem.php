<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class ArgosMovimentItem extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'MOVIMENTOITEM';

     public $timestamps = false;

    protected $fillable = [
        'ORDEMMOVIMENTO',
        'NUMEROITEM',           //37857259
        'CODIGOMATERIAL',       //0103070268
        'CODIGOBARRA',
        'CODIGOALMOXARIFADO',   //00001
        'CODIGOCENTROCUSTO',
        'CODIGOFUNCIONARIO',
        'DATA',                 //"2021-02-20 00:00:00.000"
        'QUANTIDADE',           //QTD
        'CUSTO',                //PREÇO
        'UNIDADE',
        "QUANTIDADEFATURADA",
        "FATOR",
        "FATORCOMISSAO",
        "CUSTOCALCULADO1",
        "CUSTOCALCULADO2",
        "LOTE",
        "SISTEMAORIGEM",
        "TROCAES",
        "ACAOSALDO1",
        "TRANSFERENCIA",
        "TOTALLIQUIDO",
        "BONIFICACAO",
        "ITEMVENCEDOR",
        "CODIGOTABELAPRECO",
        "PROMOCAO",
        "VALORRESGATADO",
        "CSTICMS",
        "TOTALITEM",
        "CONFIRMACONFERENCIA",
    ];

    public function moviment(): BelongsTo
    {
        return $this->belongsTo(ArgosMoviment::class, 'ordemmovimento', 'ordemmovimento');
    }

    public static function findMaterial($sku){

       return (collect(DB::connection('sqlsrv')
            ->select("
                SELECT
                    CODIGOMATERIAL, UNIDADESAIDA, CADUNIDADE.FATOR
                FROM
                    CADMATERIAL
                LEFT JOIN
                    CADUNIDADE ON CADUNIDADE.NOMENCLATURA = CADMATERIAL.UNIDADESAIDA
                WHERE CODIGOAUXILIAR = '{$sku}'"
                ))
            ->first());
    }

    public static function setLastItemNumber(): int
    {
        $lastNumber = (collect(DB::connection('sqlsrv')
            ->select("SELECT MAX(NUMEROITEM) as value FROM MOVIMENTOITEM"))
            ->first("value"));

        return $lastNumber->value += 1;
    }

}
