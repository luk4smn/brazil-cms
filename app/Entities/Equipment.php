<?php


namespace App\Entities;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Equipment extends Entity
{
    protected $table='equipments';

    protected $fillable = [
        'name',
        'quantity',
        'sector',
        'type',
        'notes'
    ];

    const
        DIRETORIA        = 1,
        ESCRITORIO       = 2,
        ESTOQUE          = 3,
        FINANCEIRO       = 4,
        LOJA_ATACADO     = 5,
        LOJA_EPOCAS      = 6,
        LOJA_PAPELARIA   = 7,
        LOJA_PLASTICOS   = 8,
        LOJA_VAREJO      = 9,
        LOJA_RECEBIMENTO = 10,
        TI               = 11;

    protected static $sectors = [
        self::DIRETORIA         => 'DIRETORIA',
        self::ESCRITORIO        => 'ESCRITÓRIO',
        self::ESTOQUE           => 'ESTOQUE',
        self::FINANCEIRO        => 'FINANCEIRO',
        self::LOJA_ATACADO      => 'LOJA ATACADO',
        self::LOJA_EPOCAS       => 'LOJA EPOCAS',
        self::LOJA_PAPELARIA    => 'LOJA PAPELARIA',
        self::LOJA_PLASTICOS    => 'LOJA PLASTICOS',
        self::LOJA_VAREJO       => 'LOJA VAREJO',
        self::LOJA_RECEBIMENTO  => 'LOJA RECEBIMENTO',
        self::TI                => 'TI',
    ];

    const
        TYPE_MONITORS        = 1,
        TYPE_PCS             = 2,
        TYPE_KEYBOARDS       = 3,
        TYPE_MOUSES          = 4,
        TYPE_CODEBAR_READERS = 5,
        TYPE_PRINTERS        = 6,
        TYPE_TONERS          = 7,
        TYPE_FONTS           = 8,
        TYPE_NOBREAKS        = 9,
        TYPE_BATTERYS        = 10,
        TYPE_DATA_COLECTORS  = 11,
        TYPE_TABLETS         = 12,
        TYPE_CAMERAS_DVR_NVR = 13,
        TYPE_HDS             = 14,
        TYPE_PROCESSORS      = 15,
        TYPE_RAM             = 16,
        TYPE_OTHERS          = 99;

    protected static $types = [
        self::TYPE_MONITORS         => 'Monitores',
        self::TYPE_PCS              => 'Computadores',
        self::TYPE_KEYBOARDS        => 'Teclados',
        self::TYPE_MOUSES           => 'Mouses',
        self::TYPE_CODEBAR_READERS  => 'Leitores de Código de Barras',
        self::TYPE_PRINTERS         => 'Impressoras',
        self::TYPE_TONERS           => 'Toners',
        self::TYPE_FONTS            => 'Fontes e Extensões',
        self::TYPE_NOBREAKS         => 'Nobreaks',
        self::TYPE_BATTERYS         => 'Baterias',
        self::TYPE_DATA_COLECTORS   => 'Coletores de dados',
        self::TYPE_TABLETS          => 'Tablets',
        self::TYPE_CAMERAS_DVR_NVR  => 'Câmeras | DVR | NVR',
        self::TYPE_HDS              => 'HDs',
        self::TYPE_PROCESSORS       => 'Processadores',
        self::TYPE_RAM              => 'Memória RAM',
        self::TYPE_OTHERS           => 'Outros'

    ];

    public static function getSectors(): array
    {
        return self::$sectors;
    }

    public function getSectorName()
    {
        if($this->attributes['sector'])
            return self::$sectors[$this->attributes['sector']];
    }

    public static function getTypes(): array
    {
        return self::$types;
    }

    public function getTypeName()
    {
        if($this->attributes['type'])
            return self::$types[$this->attributes['type']];
    }

    static function getEquipmentNameStaticly($id)
    {
        return self::findOrFail($id)->name;
    }

}
