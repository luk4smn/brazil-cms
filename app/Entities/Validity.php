<?php


namespace App\Entities;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Validity extends Entity
{
    protected $table = 'validity_controls';

    protected $fillable = [
        'provider',
        'description',
        'quantity',
        'validity_date',
        'lot',
        'status',
        'aux_code',
        'notes',
        'price'
    ];

    protected $casts = [
        'validity_date' => 'date',
    ];

    const
        STATUS_PENDING     = 1,
        STATUS_IN_PROGRESS = 2,
        STATUS_RESOLVED    = 3,

        EXPIRED = 1,
        TO_EXPIRE = 2
    ;

    protected static array $status = [
        self::STATUS_PENDING     => 'Pendente',
        self::STATUS_IN_PROGRESS => 'Em andamento',
        self::STATUS_RESOLVED    => 'Resolvido',
    ];

    public static function getStatus(): array
    {
        return self::$status;
    }

    public function getStatusName(){
        if($this->attributes['status'])
            return self::$status[$this->attributes['status']];
    }

    public function scopeSearch(Builder $builder, $query,  $params = []): Builder
    {
        if(!empty($query)){
            $builder->where(function(Builder $builder) use($query, $params) {
                return $builder
                    ->where($params[0], 'like', "%$query%")
                    ->orWhere($params[1], 'like', "%$query%")
                    ->orWhere($params[2], 'like', "%$query%")
                    ->orWhere($params[3], $query)
                    ->orWhereDate($params[4],'=', Carbon::parse(date('Y-m-d', strtotime(str_replace('/', '-', $query)))));
            });
        }

        return $builder;
    }

    public function getSituation(): string
    {
        if($this->validity_date <= now())
            return 'Vencido';

        return 'A Vencer';
    }

    public function getValidityDate()
    {
        return $this->validity_date->format('Y-m-d');
    }

}
