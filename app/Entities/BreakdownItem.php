<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 30/07/19
 * Time: 23:54
 */

namespace App\Entities;


class BreakdownItem extends Entity
{
    protected $table = 'breakdowns_items';

    protected $fillable = [
        'aux_code',
        'description',
        'unit_price',
        'quantity',
        'tax',
        'amount',
        'breakdown_id',
        'type',
        'invoice',
        'amount_with_tax'
    ];

    const
        TYPE_ITEM_BREAKDOWN = 1,
        TYPE_ITEM_MISSING   = 2,
        TYPE_ITEM_LEFTOVER  = 3;

    protected static $types = [
        self::TYPE_ITEM_BREAKDOWN   => 'Avaria',
        self::TYPE_ITEM_MISSING     => 'Falta',
        self::TYPE_ITEM_LEFTOVER    => 'Sobra',
    ];

    public function breakdown(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Breakdown::class, 'breakdown_id', 'id');
    }

    public static function getTypes(): array
    {
        return self::$types;
    }

    public function getTypeName()
    {
        if($this->attributes['type'])
            return self::$types[$this->attributes['type']];
    }

    public static function getTypeNameStaticly($type): string
    {
        return self::$types[$type];
    }

}
