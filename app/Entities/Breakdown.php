<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 30/07/19
 * Time: 23:38
 */

namespace App\Entities;


use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Breakdown extends Entity
{
    protected $table = 'breakdowns';

    protected $fillable = [
        'shipping_co',
        'provider',
        'user_id',
        'type',
        'notes',
        'status'
    ];

    const
        STATUS_PENDING     = 1,
        STATUS_IN_PROGRESS = 2,
        STATUS_RESOLVED    = 3;

    const
        TYPE_BREAKDOWN      = 1,
        TYPE_MISSING        = 2,
        TYPE_LEFTOVER       = 3,
        TYPE_BOTH_1         = 4,
        TYPE_BOTH_2         = 5,
        TYPE_BOTH_3         = 6,
        TYPE_ALL            = 7;

    protected static $status = [
        self::STATUS_PENDING     => 'Pendente',
        self::STATUS_IN_PROGRESS => 'Procedendo',
        self::STATUS_RESOLVED    => 'Resolvido',
    ];

    protected static $types = [
        self::TYPE_BREAKDOWN        => 'Avaria',
        self::TYPE_MISSING          => 'Falta',
        self::TYPE_LEFTOVER         => 'Sobra',
        self::TYPE_BOTH_1           => 'Avaria e Falta',
        self::TYPE_BOTH_2           => 'Avaria e Sobra',
        self::TYPE_BOTH_3           => 'Falta e Sobra',
        self::TYPE_ALL              => 'Avaria, Falta e Sobra',
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function items(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(BreakdownItem::class, 'breakdown_id');
    }

    public static function getStatus(): array
    {
        return self::$status;
    }

    public static function getTypes(): array
    {
        return self::$types;
    }

    public function getTypeName()
    {
        if($this->attributes['type'])
            return self::$types[$this->attributes['type']];
    }

    public function getStatusName()
    {
        if($this->attributes['status'])
            return self::$status[$this->attributes['status']];
    }

    public static function getAmountBreakdown($id)
    {
        $breakdown = Breakdown::findOrFail($id);

        return $breakdown
            ->items
            ->where('type', BreakdownItem::TYPE_ITEM_BREAKDOWN)
            ->sum('amount');
    }

    public static function getAmountWithTaxBreakDown($id)
    {
        $breakdown = Breakdown::findOrFail($id);

        return $breakdown
            ->items
            ->where('type', BreakdownItem::TYPE_ITEM_BREAKDOWN)
            ->sum('amount_with_tax');
    }

    public static function getAmountMissing($id)
    {
        $breakdown = Breakdown::findOrFail($id);

        return $breakdown
            ->items
            ->where('type', BreakdownItem::TYPE_ITEM_MISSING)
            ->sum('amount');
    }

    public static function getAmountWithTaxMissing($id)
    {
        $breakdown = Breakdown::findOrFail($id);

        return $breakdown
            ->items
            ->where('type', BreakdownItem::TYPE_ITEM_MISSING)
            ->sum('amount_with_tax');
    }

    public static function getAmountLeftOver($id)
    {
        $breakdown = Breakdown::findOrFail($id);

        return $breakdown
            ->items
            ->where('type', BreakdownItem::TYPE_ITEM_LEFTOVER)
            ->sum('amount');
    }

    public static function getAmountWithTaxLeftOver($id)
    {
        $breakdown = Breakdown::findOrFail($id);

        return $breakdown
            ->items
            ->where('type', BreakdownItem::TYPE_ITEM_LEFTOVER)
            ->sum('amount_with_tax');
    }

    public static function hasMissing($id): bool
    {
        $breakdown = Breakdown::findOrFail($id);

        switch ($breakdown->type){
            case self::TYPE_BOTH_1:
            case self::TYPE_BOTH_3:
            case self::TYPE_MISSING:
            case self::TYPE_ALL;
                return true;
            break;
        }

        return false;
    }

    public static function hasBreakdown($id): bool
    {
        $breakdown = Breakdown::findOrFail($id);

        switch ($breakdown->type){
            case self::TYPE_BOTH_1:
            case self::TYPE_BOTH_2:
            case self::TYPE_BREAKDOWN:
            case self::TYPE_ALL;
                return true;
            break;
        }

        return false;
    }

    public static function hasLeftOver($id): bool
    {
        $breakdown = Breakdown::findOrFail($id);

        return match ($breakdown->type) {
            self::TYPE_MISSING, self::TYPE_BREAKDOWN, self::TYPE_BOTH_1 => false,
            default => true,
        };

    }

}
