<?php


namespace App\Entities\Reports;


use App\Entities\Entity;
use Carbon\Carbon;

class CreditAnalysis extends Entity
{
    const EVENTCODE = [
        'BOLETO_A_VISTA'    => '00004',
        'CHEQUE_A_VISTA'    => '00002',
        'BOLETO_PRAZO'      => '00005',
        'CHEQUE_PRAZO'      => '00011'
    ];

    public static function getClient($code): string
    {
        return "select * from CADPARCEIRO c WHERE c.CODIGOPARCEIRO = {$code}";
    }

    public static function getMoviments($code, $orderMov = false): string
    {
        return "SELECT *
                FROM MOVIMENTO
                WHERE
                   (
                   MOVIMENTO.SerieNota = 'PBAL'
                   AND
                   MOVIMENTO.STATUS <> 'X'
                   AND
                   MOVIMENTO.CODIGOPARCEIRO = {$code}
                   )
                ORDER BY MOVIMENTO.DATAEMISSAO";
    }

    public static function getReleases($code, $orderMov): string
    {

        $baseSelect = "SELECT DISTINCT
                    CADLANCAMENTO.NUMEROLANCAMENTO,
                    CADPARCEIRO.DESCRICAO AS PARCEIRO_DESCRICAO,
                    CADTIPOEVENTO.CODIGOTIPOEVENTO AS CODIGOTIPOEVENTO,
                    CADTIPOEVENTO.DESCRICAO AS EVENTO_DESCRICAO,
                    CADLANCAMENTO.VALORDOCUMENTO,
                    BAIXASLANCAMENTO.VALORBAIXADO AS VALORBAIXADO,
                    CADLANCAMENTO.DATAVENCIMENTO,
                    RESUMOBAIXAS.DATAULTIMABAIXA,
                    BAIXASLANCAMENTO.DATABAIXA1 AS C_DATABAIXA1,
                    CADPORTADORTITULO.DESCRICAO AS D_DESCRICAO,
                    CADLANCAMENTO.ORDEM,
                    CADLANCAMENTO.DATAEMISSAO,
                    CADLANCAMENTO.STATUS,
                    CADLANCAMENTO.TIPO,
                    CADLANCAMENTO.SITUACAO,
                    CADLANCAMENTO.CODIGOPARCEIRO,
                    CADLANCAMENTO.ORDEMEST,
                    CADLANCAMENTO.PROTESTADO,
                    RESUMOBAIXAS.VALORBAIXADO,
                    (CASE
                        WHEN CADLANCAMENTO.CANCELADO = 'T' THEN 'C'
                        WHEN CADLANCAMENTO.SITUACAO = 'B' THEN 'B'
                        WHEN CADLANCAMENTO.SITUACAO = 'L' THEN 'L'
                        WHEN CADLANCAMENTO.SITUACAO = 'P' THEN 'P'
                        WHEN CADLANCAMENTO.STATUS = 'E' THEN 'CHQEM'
                        WHEN CADLANCAMENTO.TIPOFATURA = 'O' THEN 'O'
                        WHEN CADLANCAMENTO.TIPOFATURA = 'D' THEN 'D'
                        WHEN (CADLANCAMENTO.TIPOFATURA = 'D' AND EXISTS (
                            SELECT 1 FROM VINCULOCHEQUE Z WITH (NOLOCK)
                            WHERE Z.ORDEMCPR = CADLANCAMENTO.ORDEM AND CADLANCAMENTO.TIPO = 'R'
                        )) THEN 'DCHR'
                        WHEN EXISTS (
                            SELECT 1 FROM VINCULOCHEQUE Z WITH (NOLOCK)
                            WHERE Z.ORDEMCPR = CADLANCAMENTO.ORDEM AND CADLANCAMENTO.TIPO = 'R'
                        ) THEN 'CHR'
                        WHEN EXISTS (
                            SELECT 1 FROM VINCULOCHEQUE Z WITH (NOLOCK)
                            WHERE Z.ORDEMCPR = CADLANCAMENTO.ORDEM AND CADLANCAMENTO.TIPO = 'P'
                        ) THEN 'CHP'
                        WHEN EXISTS (
                            SELECT 1 FROM LOGUSUARIO Z WITH (NOLOCK)
                            INNER JOIN LOGUSUARIOVINCULO Y ON Z.CHAVE = CADLANCAMENTO.ORDEM
                                AND Z.TABELA = 'CADLANCAMENTO'
                                AND Z.ACAO = 'HISTORICO COBRANÇA'
                                AND Z.ORDEM = Y.ORDEM
                                AND Y.ISCOBRANCA = 'T'
                        ) THEN 'HSTC'
                        ELSE ''
                    END) AS STATUSLANCAMENTO
                FROM CADLANCAMENTO
                INNER JOIN CADPARCEIRO ON CADLANCAMENTO.CODIGOPARCEIRO = CADPARCEIRO.CODIGOPARCEIRO
                INNER JOIN CADTIPOEVENTO ON CADLANCAMENTO.CODIGOTIPOEVENTO = CADTIPOEVENTO.CODIGOTIPOEVENTO
                LEFT OUTER JOIN BAIXASLANCAMENTO ON CADLANCAMENTO.ORDEM = BAIXASLANCAMENTO.ORDEM
                INNER JOIN CADPORTADORTITULO ON CADLANCAMENTO.CODIGOPORTADORTITULO = CADPORTADORTITULO.CODIGOPORTADORTITULO
                LEFT JOIN CADADMINISTRADORACARTAO ON CADLANCAMENTO.CODIGOADMINISTRADORA = CADADMINISTRADORACARTAO.CODIGOADMINISTRADORA
                LEFT JOIN RESUMOBAIXAS ON CADLANCAMENTO.ORDEM = RESUMOBAIXAS.ORDEM
                WHERE
                    CADLANCAMENTO.TIPO = 'R'
                    AND CADLANCAMENTO.CANCELADO = 'F'
                    AND CADLANCAMENTO.STATUS = 'N'
                    AND CADPARCEIRO.CODIGOPARCEIRO = {$code}
            ";

        // Condição adicional para orderMov
        if ($orderMov) {
            $baseSelect .= "AND CADLANCAMENTO.ORDEMEST = {$orderMov}";
            $orderBy = "ORDER BY CADLANCAMENTO.NUMEROLANCAMENTO";
        } else {
            $orderBy = "ORDER BY CADLANCAMENTO.DATAVENCIMENTO";
        }

        return $baseSelect . $orderBy;
    }


    public static function getOpenValues($code): string
    {
        return sprintf("SELECT SUM(L.VALORDOCUMENTO) as VALOREMABERTO
                 FROM CADLANCAMENTO L
                 LEFT JOIN RESUMOBAIXAS ON L.ORDEM = RESUMOBAIXAS.ORDEM
                 WHERE L.CODIGOPARCEIRO = {$code}
                 AND L.CANCELADO = 'F'
                 AND L.TIPO = 'R'
                 AND L.SITUACAO <> 'B'
                 AND L.SITUACAO <> 'L'
                 AND L.SITUACAO <> 'P'
                 AND RESUMOBAIXAS.DATAULTIMABAIXA IS NULL
                 AND L.CODIGOTIPOEVENTO IN (%s, %s, %s, %s)",
            self::EVENTCODE['BOLETO_A_VISTA'],
            self::EVENTCODE['CHEQUE_A_VISTA'],
            self::EVENTCODE['BOLETO_PRAZO'],
            self::EVENTCODE['CHEQUE_PRAZO']);
    }

    public static function valueToRecive($releases)
    {
        return $releases->where('SITUACAO', '<>', 'B')
            ->where('SITUACAO', '<>', 'L')
            ->where('SITUACAO', '<>', 'P')
            ->sum('VALORDOCUMENTO');
    }

    public static function valueRecived($releases)
    {
        return $releases->where('SITUACAO', 'B')->sum('VALORDOCUMENTO');
    }

    public static function averageOverdueDays($releases): string
    {
        $diff = $releases->map(function ($item){

            $dueDate = Carbon::createFromFormat('d/m/Y', date('d/m/Y',strtotime($item->DATAVENCIMENTO)));
            $paymentDate = Carbon::createFromFormat('d/m/Y', date('d/m/Y',strtotime($item->DATAULTIMABAIXA ?? now())));

            return $dueDate > $paymentDate ? 0 : $dueDate->diffInDays($paymentDate);
        })->sum();

        $result = $diff / count($releases);
        return number_format($result, 1);
    }

    public static function overdueDays($release): int
    {
        $dueDate = Carbon::createFromFormat('d/m/Y', date('d/m/Y',strtotime($release->DATAVENCIMENTO)));
        $paymentDate = Carbon::createFromFormat('d/m/Y', date('d/m/Y',strtotime($release->DATAULTIMABAIXA ?? now())));
        return $dueDate > $paymentDate ? 0 : $dueDate->diffInDays($paymentDate);
    }


}
