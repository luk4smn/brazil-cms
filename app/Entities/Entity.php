<?php


namespace App\Entities;

use App\Traits\Searchable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entity extends Model
{
    use Searchable, SoftDeletes;

    public static function syncHasManyRelation($model, $relation, $field, $array): void
    {
        $fill = collect($array ?? [])->map(function ($item) {
            return $item;
        })->toArray();

        $model->{$relation}()->whereIn($field, array_pluck($array ?? [], $field))->delete();

        $model->{$relation}()->createMany($fill);

        $model->{$relation}()->whereNotIn($field, array_pluck($array ?? [], $field))->delete();
    }

    public function scopeFindByType(Builder $builder, $query): Builder
    {
        if(!empty($query)){
            $builder->where(function(Builder $builder) use($query) {
                return $builder
                    ->where('type', $query);
            });
        }

        return $builder;
    }

    public function scopeSearch(Builder $builder, $search,  $params = []): Builder
    {
        if(!empty($search)){
            $builder->whereLike($params, $search)
                ->orWhereDate('created_at','=', Carbon::parse(date('Y-m-d', strtotime(str_replace('/', '-', $search)))));
        }

        return $builder;
    }

}
