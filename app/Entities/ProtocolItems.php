<?php


namespace App\Entities;


class ProtocolItems extends Entity
{
    protected $table = 'protocols_items';

    protected $fillable = [
        'protocol_id',
        'equip_id',
        'quantity',
        'shipping_co',
        'shipping_date',
        'provider',
        'invoice',
        'volumes'
    ];

}
