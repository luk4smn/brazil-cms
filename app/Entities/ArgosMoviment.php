<?php


namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ArgosMoviment extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'MOVIMENTO';

    public $timestamps = false;

    protected $fillable = [
        'ORDEMMOVIMENTO',
        'CODIGOALMOXARIFADO',   //00001
        'SERIENOTA',            //PBAL
        'ID_SERIE',             //62
        'NUMEROMOVIMENTO',      //Nº do pedido
        'STATUS',               // N - T - X
        'CODIGOOPERACAO',       // 2103
        'CODIGOPARCEIRO',       // 1000001
        'CODIGOPORTADORTITULO', //00001
        'CODIGOTIPODOCUMENTO',  //00001
        'CODIGOFUNCIONARIO',
        'CODIGOTABELA1',        //101010101
        'CODIGOOPERACAOFISCAL', //5
        'CODIGOFORMAPAGAMENTO', //10001
        'CODIGOMOEDA',          //00001
        'ORDEMCAIXA',
        'DATA',
        'DATAEMISSAO',
        'DATAES',
        'DATADIGITACAO',
        'VALORMATERIAL',
        'VALORTOTAL',
        'VALORCPR',
        'TOTALLIQUIDO',
        'IPIINCLUSO',           //T
        'INCIDEIPI',            //T
        'PERCPIS',              //0.65
        'PERCCOFINS',           //3
        'DOCUMENTOORIGEM',
        'CONTABIL',             //N
        'ALIQUOTADIF',          //F
        'EXPORTADO',            //F
        'IMPRESSO',             //F
        'DUPLICATAIMPRESSA',    //F
        'CAMPOLIVREA1',
        'CAMPOLIVREA3',
        'ATUALIZACAO',
        'CODIGOCONTACORRENTE',   //1001
        'ORCAMENTOAUTORIZADO',   //F
        'ORCAMENTOANALISADO',    //F
        'EXPORTADOSUFRAMA',      //F
        'LIBERADOTIPOEVENTO',    //N
        'CODIGOMUNICIPIO',       //2504009
        'REGIMETRIBUTARIO',      //P
        'INDFINAL',              //F
        'BASEDESCONTO',          //Valor do pedido
        'EXPORTADODOMINIO',      //F
        'CODIGOTABELAPRECO',     //01001
        'CODIGOEMPRESA',         //'01'
        'CODIGOFILIALCONTABIL',  //'00001',
        'CODIGOCONTACORRENTE',   //1001
        "SISTEMAORIGEM",
        "SITUACAOFRETE",
        'PASSOULIMITECREDITO',
        'INADIPLENTE',
        "BASEICMS",
        "CODIGOSTATUS",
        "DESCONTOMOVIMENTOPENDENTE",
        "CPFCGCCONSUMIDOR",
        "VOLUME",
        "USERINCLUSAO",
        "OBSERVACAO",

    ];

    public function items()
    {
        return ArgosMovimentItem::where('ordemmovimento', $this->attributes['ORDEMMOVIMENTO'])->get();
    }

    public static function setLastOrderMoviment(): string
    {
        $table = 'MOVIMENTO';

        $lastMoviment = (collect(DB::connection('sqlsrv')
            ->select("SELECT ORDEM FROM ORDEM WHERE TABELA = '{$table}'"))
            ->first())
        ->ORDEM;

        $lastMoviment = (intval($lastMoviment)) + 1;

        DB::connection('sqlsrv')->statement("UPDATE ORDEM SET ORDEM='{$lastMoviment}' WHERE TABELA = '{$table}' ");

        return "00".$lastMoviment;
    }


    public static function getNomeFuncionario($cod){
        $table = 'CADFUNCIONARIO';

        return (collect(DB::connection('sqlsrv')
            ->select("SELECT DESCRICAO FROM {$table} WHERE CODIGOFUNCIONARIO = {$cod}"))
            ->first())->DESCRICAO
        ;
    }

}
