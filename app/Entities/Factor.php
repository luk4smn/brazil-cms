<?php


namespace App\Entities;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Factor extends Entity
{
    protected $fillable = [
        'provider',
        'factor_wl',
        'factor_cirne',
        'have_tax'
    ];

}
