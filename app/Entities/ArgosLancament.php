<?php


namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class ArgosLancament extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'CADLANCAMENTO';

     public $timestamps = false;

    protected $fillable = [
        "ORDEMEST",
        "ORDEM",
        "NUMEROLANCAMENTO",
        "TIPO",
        "CODIGOTIPODOCUMENTO",
        "CODIGOPARCEIRO",
        "HISTORICO",
        "CODIGOFUNCIONARIO",
        "CODIGOPORTADORTITULO",
        "CODIGOALMOXARIFADO",
        "CODIGOEMPRESA",
        "CODIGOFILIALCONTABIL",
        "CODIGOMOEDA",
        "CODIGOTIPOEVENTO",
        "CODIGOCONTACORRENTE",
        "VALORDOCUMENTO",
        "PROTESTO",
        "CONTABIL",
        "DATAEMISSAO",
        "DATAVENCIMENTO",
        "DATADIGITACAO",
        "DATACANCELAMENTO",
        "DATALIVRE1",
        "DATALIVRE2",
        "DATAVALIDADEDESCONTO",
        "DATAPREVISAOBAIXA",
        "DATAPREVISAO",
        "DATAREPASSE",
        "CODIGOTABELA1",
        "SISTEMAORIGEM",
        "ALTERACAOEST",
        "NUMEROTOTALPARCELA",
        "PROVISAO",
        "SITUACAO",
        "NUMEROPARCELA",
        "TIPOFATURA",
        "CANCELADO",
        "STATUS",
        "IMPRESSO",
        "PERCPIS",
        "GEROUISS",
        "GEROUINSS",
        "TEF_NSU",
        "TEF_REDE",
        //"TEF_ADMINISTRADORA"  => "00002"
        "TEF_PARCELAS",
        "TEF_TIPOTRANSACAO",
        "TEF_CODIGOTRANSACAO",
        "TEF_TIPOJUROS",
        "NUMEROCHEQUESDEVOLVIDOS",
        "GEROUCOMISSAO",
        "PROTESTADO",
        "TEF_IDRESUMO",
        "CARENCIADIASMULTA",
        "CARENCIADIASMORA",
    ];

    public function moviment(): BelongsTo
    {
        return $this->belongsTo(ArgosMoviment::class, 'ORDEMEST', 'ordemmovimento');
    }

    public static function setLastOrderMoviment(): string
    {
        $table = 'CADLANCAMENTO';

        $lastMoviment = (collect(DB::connection('sqlsrv')
            ->select("SELECT ORDEM FROM ORDEM WHERE TABELA = '{$table}'"))
            ->first())
            ->ORDEM;

        $lastMoviment = (intval($lastMoviment)) + 1;

        DB::connection('sqlsrv')->statement("UPDATE ORDEM SET ORDEM='{$lastMoviment}' WHERE TABELA = '{$table}' ");

        return "000".$lastMoviment;
    }


}
