<?php


namespace App\Entities;


use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProtocolEquipment extends ProtocolItems
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function equipment(): BelongsTo
    {
        return $this->belongsTo(Equipment::class, 'equip_id', 'id');
    }

}
