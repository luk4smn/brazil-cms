<?php


namespace App\Http\Controllers;


use App\Entities\Validity;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ValidityController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        if($status = $request->input('status', null)){
            $validities = Validity::where('status', $status)
                ->orderBy('provider')
                ->paginate();
        }
        elseif($situation = $request->input('situation', null)){
            if($situation == Validity::EXPIRED){
                $validities = Validity::whereDate('validity_date', '<=', now())
                    ->orderBy('provider')
                    ->paginate();
            }else{
                $validities = Validity::whereDate('validity_date', '>', now())
                    ->orderBy('provider')
                    ->paginate();
            }
        }
        else{
            $validities = Validity::search($request->input('q', null),['provider','description','lot','aux_code', 'validity_date'])
                ->orderBy('provider')
                ->paginate();
        }

        return view('cpd.validity_control.index', compact('validities'));
    }

    public function status(): array
    {
        return Validity::getStatus();
    }

    public function edit($id)
    {
        $validity = Validity::findOrFail($id);
        $validity->validity_date_conv = $validity->getValidityDate();

        return $validity;
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'provider'      => 'required',
            'aux_code'      => 'required',
            'description'   => 'required',
            'quantity'      => 'required',
            'validity_date' => 'required',
            'lot'           => 'required'
        ]);

        Validity::create($request->all());

        alert('Ok!', 'Dados cadastrados com sucesso', 'success');

        return back()->withInput();
    }

    public function update($id, Request $request): RedirectResponse
    {
        $validity  = Validity::findOrfail($id);

        $request->validate([
            'provider'      => 'required',
            'description'   => 'required',
            'aux_code'      => 'required',
            'quantity'      => 'required',
            'validity_date' => 'required',
            'lot'           => 'required'
        ]);

        $validity->update($request->all());

        alert('Ok!', 'Dados alterados com sucesso', 'success');

        return back()->withInput();
    }

    public function destroy($id): array
    {
        $validity  = Validity::findOrfail($id);

        if($validity)
        {
            $validity->delete();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Excluído com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        }

        return [
            'data' => [
                'status' => 'error',
                'message' => 'Não é possível excluir !',
                'info'    => 'Ocorreu um erro ao excluir esse dado',
                'code' => 401
            ]
        ];
    }

}
