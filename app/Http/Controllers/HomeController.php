<?php

namespace App\Http\Controllers;

use App\Entities\Breakdown;
use App\Entities\Validity;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $breakdowns = Breakdown::whereDate('created_at', '>=', now()->subWeek())
            ->where('status', Breakdown::STATUS_PENDING)
            ->count();

        $validities = Validity::whereDate('validity_date', '<=', now()->addDays(15))
            ->where('status', Validity::STATUS_PENDING)
            ->count();

        toast(
            '# Novas avarias/faltas na semana: '.$breakdowns.' '.
            '# Itens vencendo em 15 dias: '.$validities
            ,'info'
        )->width('26rem')
        ->position('top-right');

        return view('home');
    }
}
