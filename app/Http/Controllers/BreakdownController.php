<?php


namespace App\Http\Controllers;


use App\Entities\Breakdown;
use App\Entities\BreakdownItem;
use App\Entities\PriceDifference;
use App\Helper\Helper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BreakdownController  extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        $query = Breakdown::query();

        // Aplica filtro por status se existir
        $query->when($request->filled('status'), function ($q) use ($request) {
            $q->where('status', $request->input('status'));
        }, function ($q) use ($request) {
            // Caso não exista status, aplica a pesquisa
            $q->search($request->input('q', null), ['type','status','provider','user.name','shipping_co', 'items.invoice']);
        });

        // Ordenação padrão
        $breakdowns = $query
            ->orderBy('provider')
            ->orderBy('id', 'desc')
            ->paginate();

        return view('cpd.breakdown_and_missing.index', compact('breakdowns'));
    }

    public function types(): array
    {
        return Breakdown::getTypes();
    }

    public function itemsTypes(): array
    {
        return BreakdownItem::getTypes();
    }

    public function status(): array
    {
        return Breakdown::getStatus();
    }

    public function edit($id)
    {
        return Breakdown::findOrFail($id);
    }

    public function items($id)
    {
        $breakdown = Breakdown::findOrFail($id);

        return $breakdown->items;
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'provider'      => 'required',
            'shipping_co'   => 'required',
            'type'      => 'required',
            'status'    => 'required',
            'item'      => 'required'
        ]);

        $breakdown = Breakdown::create(array_merge($request->all(),['user_id' => auth()->user()->id]));

        if(count($items = $request->input('item'))){
            foreach($items as $item){
                $breakdown->items()->create($item);
            }
        }

        alert('Ok!', 'Dados cadastrados com sucesso', 'success');

        return back()->withInput();
    }

    public function update($id, Request $request): RedirectResponse
    {
        $breakdown  = Breakdown::findOrfail($id);

        $request->validate([
            'provider'      => 'required',
            'shipping_co'   => 'required',
            'type'      => 'required',
            'status'    => 'required',
            'item'      => 'required'
        ]);

        $breakdown->update(array_merge($request->all(),['user_id' => auth()->user()->id]));

        $breakdown->items->each(function ($item){
            $item->delete();
        });

        if(count($items = $request->input('item'))){
            foreach($items as $item){
                $breakdown->items()->create($item);
            }
        }

        alert('Ok!', 'Dados alterados com sucesso', 'success');

        return back()->withInput();
    }

    public function destroy($id): array
    {
        try {
            $breakdown = Breakdown::findOrFail($id);
            $breakdown->items->each(function ($item){
                $item->delete();
            });

            $breakdown->delete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (\Exception $exception){
            return Helper::getResponse('error', 'Erro ao realizar ação!', $exception->getMessage(), Response::HTTP_NO_CONTENT);
        }
    }

}
