<?php


namespace App\Http\Controllers;


use App\Entities\Contact;
use App\Helper\Helper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        $contacts = Contact::query();

        $contacts->when($request->filled('t'), function ($query) use ($request) {
            // Filtra por tipo se o parâmetro 't' estiver presente
            $query->findByType($request->input('t'));
        }, function ($query) use ($request) {
            // Executa a busca caso o parâmetro 't' não esteja presente
            $query->search($request->input('q', null), [
                'type',
                'name',
                'email',
                'notes',
                'telephone1',
                'telephone2',
                'telephone3',
                'cellphone1',
                'cellphone2',
                'cellphone3',
            ]);
        });

        // Ordena os resultados e realiza a paginação
        $contacts = $contacts
            ->orderBy('name')
            ->paginate();

        return view('contacts.index', compact('contacts'));
    }

    public function types(): array
    {
        return Contact::getTypes();
    }

    public function edit($id)
    {
        return Contact::findOrFail($id);
    }

    public function store(Request $request): RedirectResponse
    {

        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'email' => 'nullable|email',
        ]);

        Contact::create($request->all());

        alert('Ok!', 'Contato cadastrado com sucesso', 'success');

        return back()->withInput();
    }

    public function update($id, Request $request): RedirectResponse
    {
        $contact  = Contact::findOrfail($id);

        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'email' => 'nullable|email',
            'tel'
        ]);

        $contact->update($request->all());

        alert('Ok!', 'Contato alterado com sucesso', 'success');

        return back()->withInput();
    }

    public function destroy($id): array
    {
        try{
            $contact = Contact::findOrFail($id);
            $contact->delete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (\Exception $exception){
            return Helper::getResponse('error', 'Erro ao realizar ação!', $exception->getMessage(), Response::HTTP_NO_CONTENT);
        }
    }
}
