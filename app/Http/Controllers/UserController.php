<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index(Request $request, User $model): View|Factory|Application
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($request->input('q', null),['name','email']);
        }

        return view('users.index', ['users' => $model->paginate()]);
    }


    public function create(): Application|Factory|View
    {
        return view('users.create');
    }

    /**
     * Store a newly created user in storage
     *
     * @param UserRequest $request
     * @param User $model
     * @return RedirectResponse
     */
    public function store(UserRequest $request, User $model): RedirectResponse
    {
        $model->create($request->merge(['password' => Hash::make($request->get('password'))])->all());

        return redirect()->route('users.index')->withStatus(__('Usuário criado com sucesso.'));
    }


    public function edit(User $user): View|Factory|Application
    {
        return view('users.edit', compact('user'));
    }

    public function show(User $user): View|Factory|Application
    {
        return view('users.show', compact('user'));
    }


    public function update(UserRequest $request, User  $user)
    {
        $hasPassword = $request->get('password');
        $user->update(
            $request->merge([
                'password' => Hash::make($request->get('password'))
                ])->except([$hasPassword ? '' : 'password'])
            );

        return redirect()->route('users.index')->withStatus(__('Usuário atualizadp com sucesso.'));
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return redirect()->route('users.index')->withStatus(__('Usuário removido com sucesso.'));
    }
}
