<?php


namespace App\Http\Controllers;


use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function generate($view, Request $request): Response
    {
        $view = 'pdf.'.$view;
        $pdf = PDF::loadView($view, json_decode($request['data'], true) ?? []);

        return $pdf->stream();
    }

    public static function staticGenerate($view, $data): Response
    {
        $view = 'pdf.'.$view;
        $pdf = PDF::loadView($view, $data ?? []);

        return $pdf->stream();
    }

    public function download($view, $data = []): Response
    {
        $view = 'pdf.'.$view;

        $pdf = PDF::loadView($view, $data);
        return $pdf->download('download.pdf');
    }

}
