<?php

namespace App\Http\Controllers;

use App\Entities\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getProduct($cod_aux)
    {
        return Product::where('cod_aux', $cod_aux)->get()->first();
    }

}
