<?php


namespace App\Http\Controllers;


use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RecountController extends Controller
{

    public function index(): View|Factory|Application
    {
        return view('cpd.recount.index');
    }

    public function store(Request $request): Response|RedirectResponse
    {
        $request->validate([
            'input-xml' => 'required'
        ]);

        $data = $request['input-xml'];
        $output = [];

        foreach($data ?? [] as $key => $item){
            $filePath = $item->getRealPath();

            $mime = mime_content_type($filePath);

            if($mime == 'text/xml'){
                $file = file_get_contents($filePath);

                $xml = simplexml_load_string($file, "SimpleXMLElement", LIBXML_NOCDATA);
                $json = json_encode($xml->NFe->infNFe);
                $array = json_decode($json,TRUE);

                foreach ($array['det'] ?? [] as $index => $details){
                    if($details['prod'] ?? false){
                        $output['xml'][$key]['items'][$index] = $details['prod'] ?? '';
                    }
                }

                if($array['det']['prod'] ?? false){
                    $output['xml'][$key]['items'][0] = $array['det']['prod'] ?? '';
                }

                $output['xml'][$key]['provider'] = $array['emit'] ?? '';
                $output['xml'][$key]['shipping_co'] = $array['transp'] ?? '';
                $output['xml'][$key]['identifier'] = $array['ide'] ?? '';
            }
            else{
                return redirect()->back()->withErrors('Tipo de arquivo '.$mime.' não aceito para essa operação');
            }

        }

        return PDFController::staticGenerate('recount', $output);

    }

}
