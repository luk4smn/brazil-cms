<?php


namespace App\Http\Controllers;


use App\Entities\Equipment;
use App\Helper\Helper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class EquipmentController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        if($type = $request->input('t', null)){
            $equipments = Equipment::findByType($type)
                ->orderBy('name')
                ->paginate();
        }else{
            $equipments = Equipment::search($request->input('q', null),['name','notes','sector'])
                ->orderBy('created_at')
                ->paginate();
        }

        return view('equipments.index', compact('equipments'));
    }

    public function create(): View|Factory|Application
    {
        $sectors = Equipment::getSectors();
        $types = Equipment::getTypes();

        return view('equipments.create', compact('sectors', 'types'));
    }

    public function edit($id): View|Factory|Application
    {
        $equipment = Equipment::findOrFail($id);
        $sectors = Equipment::getSectors();
        $types = Equipment::getTypes();

        return view('equipments.edit', compact('sectors', 'types', 'equipment'));
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name'      => 'required',
            'quantity'  => 'required',
            'sector'    => 'required',
            'type'      => 'required',
            'notes'     => 'nullable'
        ]);

        Equipment::create($request->all());

        alert('Ok!', 'Equipamento cadastrado com sucesso', 'success');

        return back()->withInput();
    }

    public function update($id, Request $request): RedirectResponse
    {
        $equipment  = Equipment::findOrfail($id);

        $request->validate([
            'name'      => 'required',
            'quantity'  => 'required',
            'sector'    => 'required',
            'type'      => 'required',
            'notes'     => 'nullable'
        ]);

        $equipment->update($request->all());

        alert('Ok!', 'Dados alterados com sucesso', 'success');

        return back()->withInput();
    }

    public function destroy($id): array
    {
        try{
            $equipment  = Equipment::findOrfail($id);
            $equipment->delete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', ResponseAlias::HTTP_NO_CONTENT);
        } catch (\Exception $exception){
            return Helper::getResponse('error', 'Erro ao realizar ação!', $exception->getMessage(), ResponseAlias::HTTP_NO_CONTENT);
        }
    }

    public function list()
    {
        return Equipment::orderBy('name')
            ->get()
            ->each(function ($equip){
                return $equip->name = $equip->name.' - Localização: '.$equip->getSectorName().' - Disponíveis: '. $equip->quantity;
            })->pluck('name', 'id');
    }

}
