<?php


namespace App\Http\Controllers;


use App\Entities\Reports\CreditAnalysis;
use App\Entities\Validity;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function creditAnalysisIndex(): View|Factory|Application
    {
        return view('reports.credit_analysis');
    }

    public function creditAnalysisPost(Request $request): Response|RedirectResponse
    {
        if($code = $request->input('code')) {

            $request->validate(['code' => 'required|min:1']);

            $date1 = Carbon::now()->subYear();
            $date2 = Carbon::now();

            $queryClient = CreditAnalysis::getClient($code);
            $queryMoviments = CreditAnalysis::getMoviments($code);
            $queryReleases = CreditAnalysis::getReleases($code, null);
            $queryOpenValues = CreditAnalysis::getOpenValues($code);

            try{
                $client = DB::connection('sqlsrv')->select($queryClient);
            }catch (Exception $ex){
                return redirect()->back()->withErrors($ex->getMessage());
            }

            if(count($client)){
                $client[0]->EMABERTO = DB::connection('sqlsrv')->select($queryOpenValues);
                $moviments = DB::connection('sqlsrv')->select($queryMoviments);    // TODOS OS PEDIDOS

                $movimentsCollection = collect($moviments)
                    ->each(function ($item, $key) use($code){
                    $movReleases = collect(DB::connection('sqlsrv')
                        ->select(CreditAnalysis::getReleases($code, $item->ORDEMMOVIMENTO)))
                        ->each(function ($item){$item->CODIGOTIPOEVENTO = str_replace(' ', '', $item->CODIGOTIPOEVENTO);});

                    $item->DATAEMISSAO_CONVERTIDA = Carbon::createFromFormat('d/m/Y H:i:s', date('d/m/Y H:i:s',strtotime($item->DATAEMISSAO)));
                    $item->paymentType = $movReleases->map(function ($release){return $release->EVENTO_DESCRICAO;})->first(); // Obtem nome da forma de pagamento do pedido
                    $item->releases = $movReleases;                                                                           // Retorna os lançamentos do pedido
                    $item->releasesByType = $movReleases                                                                      // Retorna os lançamentos se eles foram boleto ou cheque
                        ->whereIn('CODIGOTIPOEVENTO', [
                            CreditAnalysis::EVENTCODE['BOLETO_A_VISTA'],
                            CreditAnalysis::EVENTCODE['CHEQUE_A_VISTA'],
                            CreditAnalysis::EVENTCODE['BOLETO_PRAZO'],
                            CreditAnalysis::EVENTCODE['CHEQUE_PRAZO']]
                        )->toArray();
                });

                $movimentsByPaymentType = $movimentsCollection->where('releasesByType', '<>', []);
                //movimentos em que os pagamentos foram com boleto ou cheque

                $releases = DB::connection('sqlsrv')->select($queryReleases);
                // Todos os lançamentos do contas a receber

                $releasesByPaymentType = collect($releases)
                    ->each(function ($item, $key){
                    $item->CODIGOTIPOEVENTO = str_replace(' ', '', $item->CODIGOTIPOEVENTO);
                    $item->DATAEMISSAO_CONVERTIDA = Carbon::createFromFormat('d/m/Y H:i:s', date('d/m/Y H:i:s',strtotime($item->DATAEMISSAO)));
                    })->whereIn('CODIGOTIPOEVENTO', [
                            CreditAnalysis::EVENTCODE['BOLETO_A_VISTA'],
                            CreditAnalysis::EVENTCODE['CHEQUE_A_VISTA'],
                            CreditAnalysis::EVENTCODE['BOLETO_PRAZO'],
                            CreditAnalysis::EVENTCODE['CHEQUE_PRAZO']]
                    );
                // Lançamentos (Cheque ou Boleto)

                $items['lastMovimentByType'] = $movimentsByPaymentType->whereBetween('DATAEMISSAO_CONVERTIDA',[$date1, $date2])
                    ->sortBy('DATAEMISSAO_CONVERTIDA')
                    ->last();
                // Última Compra nos Últimos 12 meses (Cheque ou Boleto)

                $items['maxMovimentByType'] = $movimentsByPaymentType->sortByDesc('TOTALLIQUIDO')->first();
                // Maior Compra (Cheque ou Boleto)

                $items['releasesRecived'] = $releasesByPaymentType->whereBetween('DATAEMISSAO_CONVERTIDA',[$date1, $date2])
                    ->where('DATAULTIMABAIXA', '<>', null)
                    ->sortBy('DATAVENCIMENTO');
                // Contas Recebidas nos últimos 12 meses (Cheque e Boleto)

                $items['releasesPending'] = $releasesByPaymentType->where('DATAULTIMABAIXA', null)
                    ->sortBy('DATAVENCIMENTO');
                // Contas à Receber (Cheque e Boleto)

                $items['allMovimentsLast12Months'] = $movimentsCollection->whereBetween('DATAEMISSAO_CONVERTIDA',[$date1, $date2])
                    ->reject(function ($item){
                    return $item->paymentType == 'Devolução';
                })->sortBy('DATAEMISSAO_CONVERTIDA');
                // Todos os pedidos nos ultimos 12 meses - exceto pedidos de 'troca' com forma de pagamento 'Devolução'

                $data = ['client' => $client[0], 'items' => $items];
                $view = 'pdf.credit_analysis';

                return $this->generatePDF($data, $view);

            }

            return redirect()->back()->withErrors('Parceiro não encontrado');
        }

        return redirect()->back()->withErrors('Parceiro não encontrado');
    }

    public function validitiesIndex(): View|Factory|Application
    {
        $status = Validity::getStatus();

        return view('reports.validities', compact('status'));
    }

    public function validitiesPost(Request $request): Response|RedirectResponse
    {
        $from = $request->input('validity_date_begin', false);
        $to = $request->input('validity_date_end', false);

        $items = Validity::whereBetween('validity_date', [$from, $to])->get();

        if($situation = $request->input('situation', false)){
            $items = $items->filter(function ($item, $key) use ($situation){
                if($situation == Validity::EXPIRED)
                    return $item->validity_date <= now();

                return $item->validity_date > now();
            });
        }

        if($provider = $request->input('provider', false)){
            $items = $items->filter(function ($item) use ($provider) {
                return false !== stripos($item->provider, $provider);
            });
        }

        if($description = $request->input('description', false)){
            $items = $items->filter(function ($item) use ($description) {
                return false !== stripos($item->description, $description);
            });
        }

        if($status = $request->input('status', false)){
            $items = $items->where('status', $status);
        }

        if($auxCode = $request->input('aux_code', false)){
            $items = $items->where('aux_code', $auxCode);
        }

        if(count($items)){
            $data = ['items' => $items->sortBy('validity_date'), 'from' => $from, 'to' => $to];
            $view = 'pdf.validities';

            return $this->generatePDF($data, $view);
        }

        return redirect()->back()->withErrors('Dados não encontrados');
    }

    public function generatePDF($data, $view): Response
    {
        $pdf = PDF::loadView($view, $data ?? []);
        return $pdf->stream();
    }

}
