<?php


namespace App\Http\Controllers;


use App\Entities\Files;
use App\Entities\ArgosMoviment;
use App\Entities\ArgosMovimentItem;
use App\Entities\ArgosMovimentDuplicata;


use App\Entities\ProductSelect;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;


class AdministrativeController
{
    public function sped(): View|Factory|Application
    {
        return view('adm.sped.index');
    }

    public function spedPost(Request $request): Response|RedirectResponse
    {
        $request->validate([
            'input-txt' => 'required'
        ]);

        $data = $request['input-txt'];
        $output = [];

        foreach($data ?? [] as $key => $item){
            $filePath = $item->getRealPath();

            $mime = mime_content_type($filePath);

            if($mime == 'text/plain'){
                $file = file_get_contents($filePath);

                $array = explode("\n", $file);

                foreach ($array ?? [] as $index => $line){
                    $arrayLine = explode("|", $line);
                    $arrayLine = array_filter($arrayLine);

                    if(($arrayLine[1] ?? '') == 'C170' && strlen($arrayLine[3] ?? '') > 6){
                        $output['sped'][$index] = $arrayLine;
                    }

                    if(($arrayLine[1] ?? '') == 'C425' && strlen($arrayLine[2] ?? '') > 6){
                        $output['sped'][$index] = $arrayLine;
                    }

                    if(($arrayLine[1] ?? '') == '0200' && strlen($arrayLine[2] ?? '') > 6){
                        $output['sped'][$index] = $arrayLine;
                    }
                }
            }
            else{
                return redirect()->back()->withErrors('Tipo de arquivo '.$mime.' não aceito para essa operação');
            }
        }

        if(count($output['sped'] ?? [])){
            return PDFController::staticGenerate('sped', $output);
        }else{
            return redirect()->back()->withErrors('Não existem ocorrencias para os arquivos selecionados');
        }
    }

    public function xml(Request $request): View|Factory|Application
    {
        if($request->input('type') == 'db'){
            $files = Files::groupBy('extra_field_2')->get();

            return view('adm.xml.index_db', compact('files'));
        }

        return view('adm.xml.index');
    }

    public function xmlPost(Request $request): Response|RedirectResponse
    {
        $request->validate([
            'input-xml' => 'required'
        ]);

        return self::xmlProcess($request['input-xml']);

    }

    public function xmlDatabase($id,Request $request): Response|RedirectResponse
    {
        $fileBase = Files::findOrFail($id);

        set_time_limit(0);
        $data = Files::where('extra_field_2', $fileBase->extra_field_2)->get();

        set_time_limit(0);
        return self::xmlProcess($data);
    }

    public function xmlProcess($data): Response|RedirectResponse
    {
        $output = [];

        foreach($data ?? [] as $key => $item){

            if($item instanceof UploadedFile){
                $filePath = $item->getRealPath();
                $mime = mime_content_type($filePath);
                $file = file_get_contents($filePath);
            }else{
                $file = Storage::disk('public')->get($item->path);
                $mime = $item->mime;
            }

            if($mime == 'text/xml'){
                $file = utf8_encode($file);
                $xml = simplexml_load_string($file, "SimpleXMLElement", LIBXML_NOCDATA);
                $json = json_encode($xml->NFe->infNFe);
                $array = json_decode($json,TRUE);

                foreach ($array['det'] ?? [] as $index => $details){
                    if(($details['prod'] ?? false) && (strlen($details['prod']['cProd']) > 6)){
                        //DADOS DO PRODUTO (NFE COM MAIS DE UM ITEM)
                        $output['xml'][$key]['items'][$index] = $details['prod'] ?? '';

                        //IDENTIFICAÇÃO NFE
                        $output['xml'][$key]['provider'] = $array['emit'] ?? '';
                        $output['xml'][$key]['shipping_co'] = $array['transp'] ?? '';
                        $output['xml'][$key]['identifier'] = $array['ide'] ?? '';
                        $output['xml'][$key]['file'] =  $item->path ?? $item->getClientOriginalName() ?? '';
                    }
                }

                if(($array['det']['prod'] ?? false) && (strlen($array['det']['prod']['cProd']) > 6)){
                    //DADOS DO PRODUTO (NFE COM APENAS UM ITEM)
                    $output['xml'][$key]['items'][0] = $array['det']['prod'] ?? '';

                    //IDENTIFICAÇÃO NFE
                    $output['xml'][$key]['provider'] = $array['emit'] ?? '';
                    $output['xml'][$key]['shipping_co'] = $array['transp'] ?? '';
                    $output['xml'][$key]['identifier'] = $array['ide'] ?? '';
                    $output['xml'][$key]['file'] =  $item->path ?? $item->getClientOriginalName() ?? '';
                }
            }
            else{
                return redirect()->back()->withErrors('Tipo de arquivo '.$mime.' não aceito para essa operação');
            }
        }

        if(count($output)){
            return PDFController::staticGenerate('xml_conference', $output);
        }else{
            return redirect()->back()->withErrors('Não houveram incidentes nos '.count($data).' arquivos analisados');
        }
    }

    public function csvExport(): View|Factory|Application
    {
        $types = [
            '00001' => 'PAPELARIA',
            '00002' => 'MOCHILAS',
            '00003' => 'MOCHILAS 2',
            '00004' => 'VIDRO',
            '00005' => 'BRINQUEDO IMPORTADO',
            '00006' => 'PRESENTES',
            '00007' => 'PAPELARIA - GERAL',
            '00008' => 'PAPELARIA - CADERNOS',
            '00009' => 'UTILIDADE IMPORTADA',
            '00010' => 'MATERIAL DE EXPEDIENTE',
            '00011' => 'UTILIDADE NACIONAL'
        ];

        return view('adm.csv.export',compact('types'));
    }

    public function csvPostExport(Request $request): BinaryFileResponse
    {
        $bindQuery = 'AND cadmaterial.CODIGOTABELA6 IS NOT NULL';

        if($codes = $request->input('codes')) {
            $bindQuery = '';
            foreach($codes as $key => $code){
                if($key < 1){
                    $bindQuery = 'AND cadmaterial.CODIGOTABELA6 = '.$code.' ';
                }else{
                    $bindQuery .= 'OR cadmaterial.CODIGOTABELA6 = '.$code.' ';
                }
            }
        }

        $cut = $request->input('cut') ?? 0;

        $query = ProductSelect::getQuery($bindQuery);
        $data = DB::connection('sqlsrv')->select($query);

        return ProductSelect::data_to_csv($data, $cut, '../public/produtos.csv');

    }

    public function csvImport(): View|Factory|Application
    {
        return view('adm.csv.import');
    }

    public function csvPostImport(Request $request): Response|Application|ResponseFactory
    {
         $request->validate([
            'input-csv' => 'required'
        ]);

        $data = $request['input-csv'];

        $pdo = DB::connection('sqlsrv')->getPdo();
        $pdo->exec('SET ANSI_WARNINGS ON');
        $pdo->exec('SET ANSI_PADDING ON');
        $pdo->exec('SET ANSI_NULLS ON');
        $pdo->exec('SET ARITHABORT ON');
        $pdo->exec('SET QUOTED_IDENTIFIER ON');
        $pdo->exec('SET ANSI_NULL_DFLT_ON ON');
        $pdo->exec('SET CONCAT_NULL_YIELDS_NULL ON');
        $pdo->exec('SET XACT_ABORT ON');

        foreach($data ?? [] as $key => $item){
            $filePath = $item->getRealPath();

            $mime = mime_content_type($filePath);

            if(($mime == 'text/plain') && ($item->clientExtension() == 'bin')){
                $file = file_get_contents($filePath);

                $data = str_getcsv($file, "\n");

                foreach($data as &$row) $row = str_getcsv($row, ";");

                $header = $data[0];

                $dataOutput = collect($data)->map(function ($item, $key) use($header){
                    if($key <> 0){
                        foreach ($header as $key2 => $description){
                            $description = preg_replace('/[[:^print:]]/', '', $description); //convertendo binario para string
                            $value = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/", '/[[:^print:]]/'),explode(" ","a A e E i I o O u U n N"), $item[$key2]);      //convertendo binario para string

                            if($description == "Nmero do Pedido" || $description == "Identificador do pedido" || $description == "Identificador da transao no meio de pagamento"){
                            	$output[$description] = $value;
                            }
                            else if($description == "Nome do Produto" || $description == "Valor do Produto" || $description == "Quantidade Comprada" || $description == "SKU"){
                            	$output['product'][$description] = $value;
                            }else{
                            	$output['invoice'][$description] = $value;
                            }

                        }

                        return $output;
                    }
                });

                $dataOutput->pull(0);

                $dataOutput = $dataOutput->groupBy('Nmero do Pedido');

                //$data = ArgosLancament::where('ORDEMEST', '0016073006')->get();
              	//dd($data);

                $dataOutput->each(function ($item, $invoice) use ($dataOutput) {

                	$movimentOrder = ArgosMoviment::setLastOrderMoviment();

                	$newMoviment = $item->map(function ($externalMoviment, $key) use($invoice, $movimentOrder, $dataOutput){

            			$moviment = ArgosMoviment::firstOrCreate(
	                        ['NUMEROMOVIMENTO'  => strval($invoice), 'CAMPOLIVREA1' => $dataOutput[$invoice][0]['Identificador do pedido']],
	                        [
	                            'ORDEMMOVIMENTO' 	 => $movimentOrder,
                                'CODIGOEMPRESA'         => '04',
						        'CODIGOALMOXARIFADO' => '00004', 	//00001 OR 00004
						        'SERIENOTA'			 => 'PBAL',  	//SERIE NFC OR PBAL
                                'CODIGOCONTACORRENTE'   => "1001",
                                'CODIGOFILIALCONTABIL'  => '00004',
                                'CODIGOTABELAPRECO'     => '01001',
						        'ID_SERIE'			 => '143',   	//ID 143 OR 62
						        'STATUS'			 => $externalMoviment['invoice']['Status do Pedido'] == "Cancelado" ? "X" : "N",
						        'CODIGOOPERACAO'	 => '2107', 	//2101 OR 2218
						        'CODIGOPARCEIRO'	 => '1000001',
						        'CODIGOPORTADORTITULO' 	=> '00001', //00001 OR 00004
						        'CODIGOTIPODOCUMENTO'	=> '00001',
						        'CODIGOFUNCIONARIO'		=> '04006',
						        'CODIGOTABELA1'			=> '101010101',
						        //'CODIGOOPERACAOFISCAL'	=> '5',
						        'CODIGOFORMAPAGAMENTO'	=> '10001',
						        'CODIGOMOEDA'          	=> '00001',
						        'VALORMATERIAL'			=> floatval($externalMoviment['invoice']['Subtotal']),
						        'VALORTOTAL' 			=> floatval($externalMoviment['invoice']['Subtotal']),
						        'VALORCPR'				=> floatval($externalMoviment['invoice']['Subtotal']),
						        'TOTALLIQUIDO'			=> floatval($externalMoviment['invoice']['Subtotal']),
						        "SISTEMAORIGEM" 		=> "BAL",	// BAL OU NFC
						        "SITUACAOFRETE" 		=> "F",
						        'DATA'					=> date('Y-m-d'),
						        'DATADIGITACAO'			=> date('Y-m-d'),
						        'CAMPOLIVREA3'			=> $externalMoviment['Identificador da transao no meio de pagamento'],
						        "IPIINCLUSO" 			=> "F",
	        					"INCIDEIPI" 			=> "F",
						        'ORCAMENTOAUTORIZADO' 	=> "F",
	        					'ORCAMENTOANALISADO' 	=> "F",
	        					'PASSOULIMITECREDITO' 	=> "F",
	        					'INADIPLENTE' 			=> "F",
	        					"BASEICMS" 				=> floatval($externalMoviment['invoice']['Subtotal']),
	        					"CONTABIL" 				=> "N",
	       						"ALIQUOTADIF"			=> "F",
	        					"CODIGOSTATUS" 			=> "01001",
	        					"EXPORTADOSUFRAMA" 		=> "F",
	        					"DESCONTOMOVIMENTOPENDENTE" => "F",
	        					"CPFCGCCONSUMIDOR" 		=> "00000000000",
	        					"LIBERADOTIPOEVENTO" 	=> "N",
	        					"CODIGOMUNICIPIO" 		=> "2504009",
	        					"REGIMETRIBUTARIO" 		=> "P",
	        					"INDFINAL" 				=> "T",
	        					"EXPORTADODOMINIO" 		=> "F",
	        					"VOLUME" 				=> "1.00000000",
	        					"USERINCLUSAO" 			=> "00031",
	        					"OBSERVACAO"			=> "PEDIDO REALIZADO NA PLATAFORMA E-COMMERCE"
	                        ]
                    	);

                    	ArgosMovimentItem::firstOrCreate(
                    		["ORDEMMOVIMENTO" => $moviment->ORDEMMOVIMENTO, "CODIGOMATERIAL" => ArgosMovimentItem::findMaterial($externalMoviment['product']['SKU'])->CODIGOMATERIAL],
                    		[
                    			"NUMEROITEM"			=> ArgosMovimentItem::setLastItemNumber(),
                    			"UNIDADE"				=> ArgosMovimentItem::findMaterial($externalMoviment['product']['SKU'])->UNIDADESAIDA,
       							"FATOR" 				=> ArgosMovimentItem::findMaterial($externalMoviment['product']['SKU'])->FATOR,
						        "CODIGOALMOXARIFADO" 	=> "00004", //00001 OR 00004
						        "CODIGOFUNCIONARIO" 	=> "04006",
						        "DATA" 					=> date('Y-m-d'),
						        "QUANTIDADE" 			=> $externalMoviment['product']['Quantidade Comprada'],
						        "QUANTIDADEFATURADA" 	=> $externalMoviment['product']['Quantidade Comprada'],
						        "CUSTO" 				=> floatval($externalMoviment['product']['Valor do Produto']),
						        "FATORCOMISSAO" 		=> "1.00000000",
						        "CUSTOCALCULADO1" 		=> floatval($externalMoviment['product']['Valor do Produto']),
						        "CUSTOCALCULADO2" 		=> floatval($externalMoviment['product']['Valor do Produto']),
						        "LOTE" 					=> "001",
						        "SISTEMAORIGEM" 		=> "BAL",
						        "TROCAES" 				=> "F",
						        "ACAOSALDO1" 			=> "-1.00000000",
						        "TRANSFERENCIA" 		=> "0",
						        "TOTALLIQUIDO" 			=> floatval($externalMoviment['product']['Valor do Produto']),
						        "BONIFICACAO"			=> "F",
						        "ITEMVENCEDOR" 			=> "F",
						        "CODIGOTABELAPRECO" 	=> "01001",
						        "PROMOCAO"				=> "F",
						        "VALORRESGATADO" 		=> floatval($externalMoviment['product']['Valor do Produto']),
						        "CSTICMS" 				=> "200",
						        "TOTALITEM" 			=> floatval($externalMoviment['product']['Valor do Produto']),
						        "CONFIRMACONFERENCIA" 	=> "F",
                    		]
                    	);

                    	$output['order'] = $moviment->ORDEMMOVIMENTO;
                    	$output['status'] = $moviment->STATUS;

                    	return $output;
                	});

					/*$lancament = ArgosLancament::firstOrCreate(["ORDEMEST" => $newMoviment->first()['order'], "NUMEROLANCAMENTO" => (strval($invoice).'-A1')],
                    		[
						        "ORDEM" 				=> ArgosLancament::setLastOrderMoviment(),
						        "TIPO" 					=> "R",
						        "CODIGOTIPODOCUMENTO" 	=> "00002",
						        "CODIGOPARCEIRO" 		=> "1000001",
						        "HISTORICO" 			=> 'Prov. E-COMMERCE, Doc. Nº'.strval($invoice),
						        "CODIGOFUNCIONARIO"		=> "04006",
						        "CODIGOPORTADORTITULO" 	=> "00001",
						        "CODIGOALMOXARIFADO" 	=> "00001", //00001 OR 00004
						        "CODIGOEMPRESA" 		=> "01",	//01 OR 04
						        "CODIGOFILIALCONTABIL" 	=> "00001",	//00001 OR 00004
						        "CODIGOMOEDA" 			=> "00001",
						        "CODIGOTIPOEVENTO" 		=> "00003",
						        "CODIGOCONTACORRENTE" 	=> "1001",
						        "VALORDOCUMENTO" 		=> floatval($dataOutput[$invoice][0]['invoice']['Subtotal']),
						        "PROTESTO" 				=> "0",
						        "CONTABIL" 				=> "N",
						        "DATAEMISSAO" 			=> date('Y-m-d'),
						        "DATAVENCIMENTO" 		=> now()->addMonth()->format('Y-m-d'),
						        "DATADIGITACAO" 		=> date('Y-m-d'),
						        "DATACANCELAMENTO" 		=> "1899-12-30 00:00:00.000",
						        "DATALIVRE1" 			=> "1899-12-30 00:00:00.000",
						        "DATALIVRE2" 			=> "1899-12-30 00:00:00.000",
						        "DATAVALIDADEDESCONTO" 	=> "1899-12-30 00:00:00.00",
						        "DATAPREVISAOBAIXA" 	=> "1899-12-30 00:00:00.000",
						        "DATAPREVISAO" 			=> "1899-12-30 00:00:00.000",
						        "DATAREPASSE" 			=> "1899-12-30 00:00:00.000",
						        "CODIGOTABELA1" 		=> "101010101",
						        "SISTEMAORIGEM" 		=> "NFC", //NFC OR BAL
						        "ALTERACAOEST" 			=> "F",
						        "NUMEROTOTALPARCELA" 	=> "0",
						        "PROVISAO" 				=> "F",
						        "SITUACAO" 				=> "N",
						        "NUMEROPARCELA" 		=> "1",
						        "TIPOFATURA" 			=> "N",
						        "CANCELADO" 			=> "F",
						        "STATUS" 				=> $newMoviment->first()['status'],
						        "IMPRESSO" 				=> "F",
						        "PERCPIS" 				=> "1.65000000",
						        "GEROUISS" 				=> "F",
						        "GEROUINSS" 			=> "F",
						        "TEF_NSU" 				=> "230112",
						        "TEF_REDE" 				=> "00296",
						        //"TEF_ADMINISTRADORA" 	=> "00002"
						        "TEF_PARCELAS" 			=> "01",
						        "TEF_TIPOTRANSACAO" 	=> "CR",
						        "TEF_CODIGOTRANSACAO" 	=> "1",
						        "TEF_TIPOJUROS" 		=> "L",
						        "NUMEROCHEQUESDEVOLVIDOS" => "0",
						        "GEROUCOMISSAO" 		=> "F",
						        "PROTESTADO" 			=> "F",
						        "TEF_IDRESUMO" 			=> "0",
						        "CARENCIADIASMULTA" 	=> "0",
						        "CARENCIADIASMORA" 		=> "0",
                    		]
						);*/


						$duplicata = ArgosMovimentDuplicata::firstOrCreate(["ORDEMMOVIMENTO" => $newMoviment->first()['order'], "DOCUMENTO" => (strval($invoice).'01')],
                    		[
						        //"ORDEM" 				=> $lancament->ORDEM, //vem do lancamento
						        "DATAEMISSAO" 			=> date('Y-m-d'),
						        "DATAVENCIMENTO" 		=> now()->addMonth()->format('Y-m-d'),
						        "VALORDUPLICATA" 		=> floatval($dataOutput[$invoice][0]['invoice']['Subtotal']),
						        "IMPRESSO" 				=> "F",
						        "ORIGEMATUALIZOUCPR" 	=> "F",
						        "ORIGEMATUALIZOUCC" 	=> "F",
						        "ORDEMITEM" 			=> "1",
						        "CODIGOTIPOEVENTO"		=> '00016'
                    		]
						);

						/*if($lancament->STATUS == 'X'){
							$duplicata->ORDEM = NULL;
							$duplicata->save();

							$lancament->delete();
						}*/

            	});

                return response(ResponseAlias::HTTP_OK);

            }
            else{
                return response(ResponseAlias::HTTP_UNAUTHORIZED);
            }

        }

        return response(ResponseAlias::HTTP_UNAUTHORIZED);
    }

}
