<?php


namespace App\Http\Controllers;


use App\Entities\Factor;
use App\Helper\Helper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class FactorController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        $factors = Factor::search($request->input('q', null),['provider'])
            ->orderBy('provider')
            ->paginate();

        return view('cpd.factor.index', compact('factors'));
    }

    public function edit($id)
    {
        return Factor::findOrFail($id);
    }

    public function store(Request $request): RedirectResponse
    {

        $request->validate([
            'provider' => 'required',
            'factor_wl' => 'nullable',
            'factor_cirne' => 'nullable',
        ]);

        Factor::create($request->all());

        alert('Ok!', 'Fator cadastrado com sucesso', 'success');

        return back()->withInput();
    }

    public function update($id, Request $request): RedirectResponse
    {
        $factor  = Factor::findOrfail($id);

        $request->validate([
            'provider' => 'required',
            'factor_wl' => 'nullable',
            'factor_cirne' => 'nullable',
        ]);

        $factor->update($request->all());

        alert('Ok!', 'Fator alterado com sucesso', 'success');

        return back()->withInput();
    }

    public function destroy($id): array
    {
        try{
            $factor = Factor::findOrFail($id);
            $factor->delete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', ResponseAlias::HTTP_NO_CONTENT);
        } catch (\Exception $exception){
            return Helper::getResponse('error', 'Erro ao realizar ação!', $exception->getMessage(), ResponseAlias::HTTP_NO_CONTENT);
        }
    }

}
