<?php


namespace App\Http\Controllers;


use App\Entities\Protocol;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class ProtocolController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        $type = $request->input('t', null);

        if($date = $request->input('q', null)){
            $protocols = Protocol::findByType($type)
                ->whereDate('created_at','=', Carbon::parse(date('Y-m-d', strtotime(str_replace('/', '-', $date)))))
                ->orderBy('created_at', 'desc')
                ->paginate();
        }else{
            $protocols = Protocol::findByType($type)
                ->orderBy('created_at', 'desc')
                ->paginate();
        }

        if($type == Protocol::TYPE_EQUIPMENT)
            $view = 'equipments';
        else
            $view = 'products';

        return view('protocols.'.$view.'.index', compact('protocols'));
    }

    public function edit($id)
    {
        return Protocol::findOrFail($id);
    }

    public function status(): array
    {
        return Protocol::getStatus();
    }

    public function items($id)
    {
        $protocols = Protocol::findOrFail($id);

        return $protocols->items;
    }

    public function store(Request $request): Redirector|RedirectResponse|Application
    {
        $request->validate([
            'recipient' => 'nullable',
            'notes' => 'nullable',
            'type' => 'required',
        ]);

        $type = $request->input('type', null);

        $protocol = Protocol::create(array_merge($request->all(),['user_id' => auth()->user()->id]));

        if(count($items = $request->input('item'))){
            foreach($items as $item){
                $protocol->items()->create($item);
            }
        }

        alert('Ok!', 'Protocolo cadastrado com sucesso', 'success');

        return redirect(route('protocols.index', ['t' => $type] ));
    }

    public function update($id, Request $request): Redirector|RedirectResponse|Application
    {
        $protocol  = Protocol::findOrfail($id);

        $request->validate([
            'recipient' => 'nullable',
            'notes' => 'nullable',
            'type' => 'required',
        ]);

        $type = $request->input('type', null);

        $protocol->update(array_merge($request->all(),['user_id' => auth()->user()->id]));

        $protocol->items->each(function ($item){
            $item->delete();
        });

        if(count($items = $request->input('item'))){
            foreach($items as $item){
                $protocol->items()->create($item);
            }
        }

        alert('Ok!', 'Dados alterados com sucesso', 'success');

        return redirect(route('protocols.index', ['t' => $type] ));
    }

    public function destroy($id): array
    {
        $protocol  = Protocol::findOrfail($id);

        if($protocol)
        {
            $protocol->delete();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Excluído com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        }

        return [
            'data' => [
                'status' => 'error',
                'message' => 'Não é possível excluir !',
                'info'    => 'Ocorreu um erro ao excluir esse dado',
                'code' => 401
            ]
        ];
    }

}
