<?php


namespace App\Http\Controllers;

use App\Entities\PriceDifference;
use App\Helper\Helper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PriceDifferenceController extends Controller
{
    public function index(Request $request): View|Factory|Application
    {
        $query = PriceDifference::query();

        // Aplica filtro por status se existir
        $query->when($request->filled('status'), function ($q) use ($request) {
            $q->where('status', $request->input('status'));
        }, function ($q) use ($request) {
            // Caso não exista status, aplica a pesquisa
            $q->search($request->input('q', null), ['provider', 'invoice', 'user.name', 'items.aux_code', 'items.description', 'items.ref']);
        });

        // Ordenação padrão
        $pricesDifferences = $query
            ->orderBy('provider')
            ->orderBy('id', 'desc')
            ->paginate();

        return view('cpd.price_difference.index', compact('pricesDifferences'));
    }


    public function status(): array
    {
        return PriceDifference::getStatus();
    }

    public function edit($id)
    {
        return PriceDifference::findOrFail($id);
    }

    public function items($id)
    {
        $difference = PriceDifference::findOrFail($id);

        return $difference->items;
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'provider'  => 'required',
            'invoice'   => 'required',
            'status'    => 'required',
            'item'      => 'required'
        ]);

        $price = PriceDifference::create(array_merge($request->all(),['user_id' => auth()->user()->id]));

        if(count($items = $request->input('item'))){
            foreach($items as $item){
                $price->items()->create($item);
            }
        }

        alert('Ok!', 'Dados cadastrados com sucesso', 'success');

        return back()->withInput();
    }

    public function update($id, Request $request): RedirectResponse
    {
        $price  = PriceDifference::findOrfail($id);

        $request->validate([
            'provider'  => 'required',
            'invoice'   => 'required',
            'status'    => 'required',
            'item'      => 'required'
        ]);

        $price->update(array_merge($request->all(),['user_id' => auth()->user()->id]));

        $price->items->each(function ($item){
            $item->delete();
        });

        if(count($items = $request->input('item'))){
            foreach($items as $item){
                $price->items()->create($item);
            }
        }

        alert('Ok!', 'Dados alterados com sucesso', 'success');

        return back()->withInput();
    }

    public function destroy($id): array
    {
        try {
            $price = PriceDifference::findOrFail($id);
            $price->items->each(function ($item){
                $item->delete();
            });

            $price->delete();

            return Helper::getResponse('success', 'Excluído com sucesso!', 'Recarregando em 2 segundos...', Response::HTTP_NO_CONTENT);
        } catch (\Exception $exception){
            return Helper::getResponse('error', 'Erro ao realizar ação!', $exception->getMessage(), Response::HTTP_NO_CONTENT);
        }
    }

}
