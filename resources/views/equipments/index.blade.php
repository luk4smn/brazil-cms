@extends('layouts.app', [
    'namePage' => 'Equipamentos',
    'class' => 'sidebar-mini',
    'activePage' => 'equipments',
    'activeNav' => '',
    'searchRoute' => route('equipments.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Equipamentos</h2>
            <p class="category">Lista de itens cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right" href="{{ route('equipments.create') }}">{{ '+' }}
                        </a>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table" cellspacing="0" width="100%">
                            <thead class=" text-primary">
                            <tr>
                            <tr>
                                <th><strong>Nome</strong></th>
                                <th><strong>Quantidade</strong></th>
                                <th><strong>Tipo</strong></th>
                                <th><strong>Setor</strong></th>
                                <th><strong>OBS.</strong></th>
                                <th class="disabled-sorting text-center"><strong>{{ __('Ações') }}</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($equipments as $equipment)
                                <tr>
                                    <td style="font-size: small">{{ $equipment->name }}</td>
                                    <td style="font-size: small">{{ $equipment->quantity }}</td>
                                    <td style="font-size: small">{{ $equipment->getTypeName() }}</td>
                                    <td style="font-size: small">{{ $equipment->getSectorName() }}</td>
                                    <td style="font-size: smaller">{{ $equipment->notes ?? '' }}</td>
                                    <td id="bread-actions" align="center">
                                        <a title="Editar" class="btn btn-primary btn-icon btn-sm pull-center" href="{{ route('equipments.edit', $equipment->id) }}">
                                            {!! '&#9997;' !!}
                                        </a>

                                        <button title="Delete" class="btn btn-icon btn-sm btn-danger pull-center delete"
                                                href="#"
                                                data-action="{{ route('equipments.destroy', $equipment->id) }}"
                                                data-redirect="{{ route('equipments.index') }}"
                                        >
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" align="center">Nenhum dado cadastrado até o momento</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $equipments->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
