@extends('layouts.app', [
    'namePage' => 'Equipamentos',
    'class' => 'sidebar-mini',
    'activePage' => 'equipments',
    'activeNav' => '',
    'searchRoute' => route('equipments.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Alteração de cadastro de equipamentos</h2>
            <p class="category">Registro de equipamentos da loja</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form action="{{ route('equipments.update', $equipment->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="PUT">

                                    @csrf
                                    <div class="form-example-wrap">
                                        <div class="form-example-int">
                                            <div class="form-group">
                                                <label class="text-primary"><strong>Nome</strong></label>
                                                <div class="nk-int-st">
                                                    <input type="text" class="form-control input-sm" name="name" value="{{ $equipment->name ?? null }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-example-int mg-t-15">
                                            <div class="form-group">
                                                <label class="text-primary"><strong>Quantidade</strong></label>
                                                <div class="nk-int-st">
                                                    <input type="number" class="form-control input-sm" name="quantity" value="{{ $equipment->quantity ?? null }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-example-int mg-t-15">
                                            <div class="form-group cmp-tb-hd mg-t-20">
                                                <label class="text-primary" for="sector"><strong>Tipo</strong></label>
                                                <div class="nk-int-st">
                                                    <select class="form-control" id="sector" name="type" required>
                                                        <option value="">Selecione uma opção</option>
                                                        @foreach($types as $key => $name)
                                                            @if($equipment->type == $key)
                                                                <option value="{{ $key }}" selected>{{ $name }}</option>
                                                            @else
                                                                <option value="{{ $key }}">{{ $name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-example-int mg-t-15">
                                            <div class="form-group cmp-tb-hd mg-t-20">
                                                <label class="text-primary" for="sector"><strong>Setor</strong></label>
                                                <div class="nk-int-st">
                                                    <select class="form-control" id="sector" name="sector" required>
                                                        <option value="">Selecione uma opção</option>
                                                        @foreach($sectors as $key => $name)
                                                            @if($equipment->sector == $key)
                                                                <option value="{{ $key }}" selected>{{ $name }}</option>
                                                            @else
                                                                <option value="{{ $key }}">{{ $name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-example-int mg-t-15">
                                            <div class="form-group cmp-tb-hd mg-t-20">
                                                <label class="text-primary" for="notes"><strong>Observações</strong></label>
                                                <div class="nk-int-st">
                                                    <textarea type="text" name="notes" class="form-control" id="notes">{{ $equipment->notes ?? null }}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-example-int mg-t-15">
                                            <button type="submit" class="btn btn-primary">Salvar mudanças</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
