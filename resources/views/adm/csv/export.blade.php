@extends('layouts.app', [
    'namePage' => 'Exportar Catálogo',
    'class' => 'sidebar-mini',
    'activePage' => 'csv-export',
    'activeNav' => '',
    'searchRoute' => route('contacts.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Exportação de catálogo em CSV</h2>
            <p class="category">Gerar arquivo CSV a partir de registros do ARGOS para e-commerce</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form action="{{ route('admin.csvPost_export') }}" method="post" enctype="multipart/form-data">
                                    @csrf()
                                    <div>
                                        <div class="form-group">
                                            <label class="text-primary" for="recipient">
                                                <strong>Quantidade de corte (Exibir na loja online a partir de X no estoque)</strong>
                                            </label>
                                            <input class="form-control input-sm" type="number" name="cut" id="recipient" min="0" value="50">
                                        </div>

                                        <div class="form-group">
                                            <label class="text-primary" for="codes">
                                                <strong>Códigos de catálogo</strong>
                                            </label>
                                            <select multiple class="form-control" id="codes" name="codes[]" size="15">
                                                <option value="" disabled selected>TODOS</option>
                                                @foreach($types as $key => $type)
                                                    <option value="{{$key}}">{{$key}} - {{ $type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- cellphone div -->
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Exportar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection


