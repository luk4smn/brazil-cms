@extends('layouts.app', [
    'namePage' => 'Importar Pedido',
    'class' => 'sidebar-mini',
    'activePage' => 'csv-import',
    'activeNav' => '',
    'searchRoute' => route('contacts.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Importação de pedido em CSV</h2>
            <p class="category">Importar arquivo CSV a partir de registros e-commerce para o ARGOS</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form action="{{ route('admin.csvPost_import') }}" method="post" enctype="multipart/form-data">
                                    @csrf()
                                    <input id="input-async" name="input-csv[]" type="file" multiple>
<!--                                    <input id="input-csv" name="input-csv[]" type="file" class="file" data-browse-on-zone-click="true" false>-->
                                </form>
                                <!-- end content-->
                            </div>
                        </div>


                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
