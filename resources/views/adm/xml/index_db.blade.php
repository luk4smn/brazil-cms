@extends('layouts.dashboard.template')

@section('search')
    <form class="contact-form" action="{{ route('contacts.index') }}" method="GET">
        @csrf
        <div class="search-input">
            <i class="notika-icon notika-left-arrow"></i>
            <input type="text" name=q placeholder="Bucar Contato" />
        </div>
    </form>
@endsection

@section('content')
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-calendar"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Análise de arquivo XML (no banco de dados)</h2>
                                    <p>Itens cadastrados no sistema</p>
                                </div>
                            </div>
                        </div>
                        <div class="basic-tb-hd">
                            <h2 style="color: white">.</h2>
                        </div>

                        <!-- <div class="table-responsive"> -->
                        <div>
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ANO</th>
                                    <th>TIPO</th>
                                    <th>LOTE</th>
                                    <th>QTD ARQUIVOS</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($files as $file)
                                <tr>
                                    <td style="font-size: small">{{ $file->getYearXML() }}</td>
                                    <td style="font-size: small">{{ $file->extra_field_1 }}</td>
                                    <td style="font-size: small">{{ $file->extra_field_2 }}</td>
                                    <td style="font-size: small">{{ $file->countXML() }}</td>
                                    <td id="bread-actions" align="center">
                                        <form action="{{ route('admin.xmlDb', $file->id) }}" method="post" enctype="multipart/form-data">
                                            @csrf()
                                            <button class="btn btn-sm btn-primary" type="submit">
                                                <i class="not-active notika-icon notika-sent" disabled="true"></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                                @empty
                                    <tr>
                                        <td colspan="8" align="center">Nenhum dado cadastrado até o momento</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->

    <contact></contact>


@endsection

@push('scripts')
    <script>
        changeMenuTab("administrative");
    </script>
@endpush
