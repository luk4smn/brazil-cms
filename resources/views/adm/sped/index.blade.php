@extends('layouts.dashboard.template')

@section('search')
    <form class="contact-form" action="{{ route('breakdowns.index') }}" method="GET">
        @csrf
        <div class="search-input">
            <i class="notika-icon notika-left-arrow"></i>
            <input type="text" name=q placeholder="Bucar Avaria / Falta" />
        </div>
    </form>
@endsection

@section('content')
    <!-- Dropzone area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-paperclip"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Conferência de arquivo txt SPED</h2>
                                    <p>Envio de arquivos</p>
                                </div>
                            </div>
                        </div>
                        <div class="basic-tb-hd">
                            <h2 style="color: white">.</h2>
                        </div>
                        <div class="basic-tb-hd">
                            <h2 style="color: white">.</h2>
                        </div>
                        <form action="{{ route('admin.spedPost') }}" method="post" enctype="multipart/form-data">
                            @csrf()
                            <input id="input-txt" name="input-txt[]" type="file" class="file" data-browse-on-zone-click="true" multiple>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Dropzone area End-->


@endsection

@push('scripts')
    <script>
        changeMenuTab("administrative");
    </script>
@endpush
