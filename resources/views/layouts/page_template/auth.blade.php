
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
@include('layouts.navbars.sidebar')
<div class="main-panel ps ps--active-y" id="main-panel">
    @include('layouts.navbars.navs.auth')
    <div id="app">
        @yield('content')
    </div>
    @include('layouts.footer')
</div>
