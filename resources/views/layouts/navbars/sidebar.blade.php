<div class="sidebar" data-color="blue">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
-->
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
            <img src="{{ asset('assets/img/now-logo.png') }}" alt="" width="30" height="30">
        </a>
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            {{ env('APP_NAME') ?? "Brazil-CMS" }}
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li class="@if ($activePage == 'home') active @endif">
                <a href="{{ route('home') }}">
                    <i class="now-ui-icons design_app"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class = "nav-item dropdown @if ($activePage == 'contacts') active @endif">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="now-ui-icons education_agenda-bookmark"></i>
                    <p>{{ __('Contatos') }}</p>
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('contacts.index') }}"><strong>Todos</strong></a>
                    <a class="dropdown-item" href="{{ route('contacts.index', ['t' => \App\Entities\Contact::COLABORADOR]) }}">Colaboradores</a>
                    <a class="dropdown-item" href="{{ route('contacts.index', ['t' => \App\Entities\Contact::FORNECEDOR]) }}">Fornecedores</a>
                    <a class="dropdown-item" href="{{ route('contacts.index', ['t' => \App\Entities\Contact::REPRESENTANTE]) }}">Representantes</a>
                    <a class="dropdown-item" href="{{ route('contacts.index', ['t' => \App\Entities\Contact::PROVEDOR_DE_SERVICO]) }}">Provedores de Serviços</a>
                    <a class="dropdown-item" href="{{ route('contacts.index', ['t' => \App\Entities\Contact::EMPRESA_GRUPO]) }}">Empresas do grupo</a>
                    <a class="dropdown-item" href="{{ route('contacts.index', ['t' => \App\Entities\Contact::SUPORTE]) }}">Suporte</a>
                </div>
            </li>
            <li class = "nav-item dropdown @if ($activePage == 'cpd') active @endif">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="now-ui-icons business_briefcase-24"></i>
                    <p>{{ __('CPD') }}</p>
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('breakdowns.index') }}">Avaria e falta</a>
                    <a class="dropdown-item" href="{{ route('recounts.index') }}">Conferência manual</a>
                    <a class="dropdown-item" href="{{ route('validities.index') }}">Controle de validades</a>
                    <a class="dropdown-item" href="{{ route('price-differences.index') }}">Diferênça de preços</a>
                    <a class="dropdown-item" href="{{ route('factors.index') }}">Fatores</a>
                </div>
            </li>
            <li class = "nav-item dropdown @if ($activePage == 'equipments') active @endif">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="now-ui-icons tech_laptop"></i>
                    <p>{{ __('Equipamentos') }}</p>
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('equipments.index') }}"><strong>Todos</strong></a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_MONITORS]) }}">Monitores</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_PCS]) }}">Computadores</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_KEYBOARDS]) }}">Teclados</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_MOUSES]) }}">Mouses</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_CODEBAR_READERS]) }}">Leitores</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_PRINTERS]) }}">Impressoras</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_TONERS]) }}">Toners</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_FONTS]) }}">Fontes</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_NOBREAKS]) }}">Nobreaks</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_BATTERYS]) }}">Baterias</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_DATA_COLECTORS]) }}">Coletores</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_TABLETS]) }}">Tablets</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_CAMERAS_DVR_NVR]) }}">Cameras</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_HDS]) }}">HDs</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_PROCESSORS]) }}">Processadores</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_RAM]) }}">Memória</a>
                    <a class="dropdown-item" href="{{ route('equipments.index', ['t' => \App\Entities\Equipment::TYPE_OTHERS]) }}">Outros</a>
                </div>
            </li>
            <li class = "nav-item dropdown @if ($activePage == 'protocols') active @endif">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="now-ui-icons files_single-copy-04"></i>
                    <p>{{ __('Protocolos') }}</p>
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('protocols.index', ['t' => \App\Entities\Protocol::TYPE_EQUIPMENT]) }}">Protocolo de Equipamentos</a>
                    <a class="dropdown-item" href="{{ route('protocols.index', ['t' => \App\Entities\Protocol::TYPE_PRODUCT]) }}">Protocolo de Entrada de Mercadoria</a>
                </div>
            </li>
            <li class = "nav-item dropdown @if ($activePage == 'reports') active @endif">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="now-ui-icons files_box"></i>
                    <p>{{ __('Relatórios') }}</p>
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('credit-analysis.index') }}">Análise de Crédito</a>
                    <a class="dropdown-item" href="{{ route('validities-report.index') }}">Controle de Validades</a>
                </div>
            </li>
            <li class = " @if ($activePage == 'csv-export') active @endif">
                <a href="{{ route('admin.csv_export') }}">
                    <i class="now-ui-icons shopping_box"></i>
                    <p>{{ __('Exportação de Catálogo') }}</p>
                </a>
            </li>
            @if(auth()->user()->is_admin)
                <li class = " @if ($activePage == 'csv-import') active @endif">
                    <a href="{{ route('admin.csv_import') }}">
                        <i class="now-ui-icons shopping_bag-16"></i>
                        <p>{{ __('Importação de Pedidos') }}</p>
                    </a>
                </li>


            <li class = " @if ($activePage == 'users') active @endif">
                    <a href="{{ route('users.index') }}">
                        <i class="now-ui-icons users_circle-08"></i>
                        <p>{{ __('Usuários') }}</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
