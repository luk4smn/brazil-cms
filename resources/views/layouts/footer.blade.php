<footer class="footer navbar-fixed-bottom">
  <div class=" container-fluid ">
    {{--<nav>
      <ul>
        <li>
          <a href="https://www.creative-tim.com" target="_blank">
            {{__(" Site")}}
          </a>
        </li>
        <li>
          <a href="http://presentation.creative-tim.com" target="_blank">
            {{__(" Facebook")}}
          </a>
        </li>
        <li>
          <a href="http://blog.creative-tim.com" target="_blank">
            {{__(" Instagram")}}
          </a>
        </li>
        <li>
          <a href="https://www.updivision.com" target="_blank">
            {{__(" Blog")}}</a>
        </li>
      </ul>
    </nav>--}}
    <div class="copyright" id="copyright">
      &copy;
      <script>
        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
      </script>, {{__(" Designed and coded by")}}
      <a href="https://www.linkedin.com/in/luk4smn/" target="_blank">{{__(" luk4smn")}}</a>
    </div>
  </div>
</footer>
