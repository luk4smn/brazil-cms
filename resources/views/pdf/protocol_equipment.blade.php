<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <style type="text/css">
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }

            header {
                padding: 10px 0;
                margin-bottom: 20px;
                border-bottom: 1px solid #AAAAAA;
            }

            #company {
                text-align: left;
            }

            #begin {
                margin-bottom: 5px;
                border-bottom: 2px solid #AAAAAA;
            }

            .text-small {
                font-size: x-small;
            }

        </style>



        <title>PROTOCOLO DE SAÍDA DE EQUIPAMENTOS</title>
    </head>
    <body>

    <header class="clearfix">
        <div id="company">
            <h5>WL Comércio e importação LTDA</h5>
            <div class="text-small">Rua Presidente João Pessoa, 287 - Centro</div>
            <div class="text-small">CEP: 58400-002, Campina Grande - PB</div>
            <div class="text-small">Tel: (83) 3321-2828</div>
        </div>
    </header>

    <h4 align="center">PROTOCOLO DE SAÍDA DE EQUIPAMENTOS</h4>
    <br>
    <div>
        <p class="text-small"><strong>Recebedor:</strong> {{ $recipient ?? '' }}</p>
        <p class="text-small"><strong>Observações:</strong> {{ $notes ?? '' }}</p>
        <p class="text-small"><strong>Data:</strong> {{ \Carbon\Carbon::parse($created_at)->format("d/m/Y") }}</p>
    </div>

    <div class="table-responsive" id="begin">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align: center">QTD</th>
                <th style="text-align: center">ITEM</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td align="center">{{ $item['quantity'] ?? '' }}</td>
                    <td align="center">{{ \App\Entities\Equipment::getEquipmentNameStaticly($item['equip_id']) ?? '' }}</td>
                </tr>
            @endforeach
            @if(count($items) < 6)
                @for($i = count($items); $i < 6; $i++)
                    <tr>
                        <td align="center" style="color: white;">.</td>
                        <td align="center" style="color: white;">.</td>
                    </tr>
                @endfor
            @endif
            </tbody>
        </table>
    </div>

    <br>

    <h4 align="center">PROTOCOLO DE SAÍDA DE EQUIPAMENTOS</h4>
    <br>
    <div>
        <p class="text-small"><strong>Recebedor:</strong> {{ $recipient ?? '' }}</p>
        <p class="text-small"><strong>Observações:</strong> {{ $notes ?? '' }}</p>
        <p class="text-small"><strong>Data:</strong> {{ \Carbon\Carbon::parse($created_at)->format("d/m/Y") }}</p>
    </div>

    <div class="table-responsive" id="end">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align: center">QTD</th>
                <th style="text-align: center">ITEM</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td align="center">{{ $item['quantity'] ?? '' }}</td>
                    <td align="center">{{ \App\Entities\Equipment::getEquipmentNameStaticly($item['equip_id']) ?? '' }}</td>
                </tr>
            @endforeach
            @if(count($items) < 6)
                @for($i = count($items); $i < 6; $i++)
                    <tr>
                        <td align="center" style="color: white;">.</td>
                        <td align="center" style="color: white;">.</td>
                    </tr>
                @endfor
            @endif
            </tbody>

            <tfoot align="center">
                <tr>
                    <td>
                        Conferente
                    </td>
                    <td>
                        Visto Gerente
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

    </body>
</html>
