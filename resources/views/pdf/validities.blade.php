<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #company {
            text-align: left;
        }

        .text-small {
            font-size: x-small;
        }
        @page {
            size: A4 landscape;
        }

    </style>

    <title>CONTROLE DE VALIDADES</title>
</head>
<body>
<header class="clearfix">
    <div id="company">
        <h5>WL Comércio e importação LTDA</h5>
        <div class="text-small">Rua Presidente João Pessoa, 287 - Centro</div>
        <div class="text-small">CEP: 58400-002, Campina Grande - PB</div>
        <div class="text-small">Tel: (83) 3321-2828</div>
    </div>
</header>
    <h2 align="center">CONTROLE DE VALIDADES</h2>
    <br>
    <div>
        <p class="text-small">
            <strong>De:</strong> {{ \Carbon\Carbon::parse($from)->format("d/m/Y") }}
            <strong>Até:</strong> {{ \Carbon\Carbon::parse($to)->format("d/m/Y") }}
        </p>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align: center">COD. AUX</th>
                <th style="text-align: center">DESCRIÇÃO</th>
                <th style="text-align: center">FORNECEDOR</th>
                <th style="text-align: center">QTD</th>
                <th style="text-align: center">VALIDADE</th>
                <th style="text-align: center">SITUAÇÃO</th>
                <th style="text-align: center">PREÇO DE VENDA</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td align="center">{{ $item['aux_code'] }}</td>
                    <td align="left">{{ $item['description'] }}</td>
                    <td align="left">{{ $item['provider'] }}</td>
                    <td align="center">{{ $item['quantity'] }}</td>
                    <td align="center">{{ \Carbon\Carbon::parse($item['validity_date'])->format("d/m/Y") }}</td>
                    <td align="center">{{ ($item['validity_date'] <= now() ? 'Vencido': 'A Vencer') }}</td>
                    <td align="center">@money($item['price'])</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot align="right">
            </tfoot>
        </table>
    </div>

</body>
</html>
