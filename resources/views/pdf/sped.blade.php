<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <style type="text/css">
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }

            header {
                padding: 10px 0;
                margin-bottom: 20px;
                border-bottom: 1px solid #AAAAAA;
            }

            #company {
                text-align: left;
            }

            .text-small {
                font-size: x-small;
            }
            @page {
                size: A4 landscape;
            }
        </style>

        <title>CONFERENCIA DE TXT SPED</title>
    </head>
    <body>
    <header class="clearfix">
        <div id="company">
            <h5>WL Comércio e importação LTDA</h5>
            <div class="text-small">Rua Presidente João Pessoa, 287 - Centro</div>
            <div class="text-small">CEP: 58400-002, Campina Grande - PB</div>
            <div class="text-small">Tel: (83) 3321-2828</div>
        </div>
    </header>

    <h2 align="center">CONFERENCIA DE TXT SPED</h2>
        <br><br>
        @forelse($sped as $line)

            <p class="text-small">
                {{ implode('|', $line) }}
            </p>
        </div>

        @empty
        <div>
            <p class="text-small">
                {{ 'Não houveram ocorrencias para os arquivos enviados' }}
            </p>
        </div>
        @endforelse
</body>
</html>
