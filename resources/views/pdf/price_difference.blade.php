<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <style type="text/css">
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }

            header {
                padding: 10px 0;
                margin-bottom: 20px;
                border-bottom: 1px solid #AAAAAA;
            }

            #company {
                text-align: left;
            }

            .text-small {
                font-size: x-small;
            }

        </style>



        <title>DIFERENÇA DE PREÇOS #{{ $id }}</title>
    </head>
    <body>
    <header class="clearfix">
        <div id="company">
            <h5>WL Comércio e importação LTDA</h5>
            <div class="text-small">Rua Presidente João Pessoa, 287 - Centro</div>
            <div class="text-small">CEP: 58400-002, Campina Grande - PB</div>
            <div class="text-small">Tel: (83) 3321-2828</div>
        </div>
    </header>


        <h2 align="center">DIFERENÇA DE PREÇOS</h2>
        <br>
        <div>
            <p class="text-small">Fornecedor: {{ $provider }}</p>
            <p class="text-small">NF-e: {{ $invoice }}</p>
            <p class="text-small">Data: {{ \Carbon\Carbon::parse($created_at)->format("d/m/Y") }}</p>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-sm text-small">
                <thead>
                <tr>
                    <th style="text-align: center">ID</th>
                    <th style="text-align: center">REF</th>
                    <th style="text-align: center">DESCRIÇÃO</th>
                    <th style="text-align: center">QTD</th>
                    <th style="text-align: center">R$ PEDIDO</th>
                    <th style="text-align: center">R$ NF-e</th>
                    <th style="text-align: center">IPI</th>
                    <th style="text-align: center">DIF</th>
                    <th style="text-align: center">TOTAL DIF</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td align="center">{{ $item['aux_code'] }}</td>
                        <td align="center">{{ $item['ref'] }}</td>
                        <td align="left">{{ $item['description'] }}</td>
                        <td align="center">{{ $item['quantity'] }}</td>
                        <td align="right">@money( $item['original_price'] )</td>
                        <td align="right">@money( $item['recived_price'] )</td>
                        <td align="center">{{ $item['tax'] }}%</td>
                        <td align="right">@money( $item['recived_price'] -  $item['original_price'] )</td>
                        <td align="center">@money( $item['amount'])</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot align="right">
                <tr>
                    <td colspan="4"></td>
                    <td colspan="4"><strong>TOTAL</strong></td>
                    <td>@money( \App\Entities\PriceDifference::getAmount($id) )</td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                    <td colspan="4"><strong>TOTAL C/ IPI</strong></td>
                    <td>@money( \App\Entities\PriceDifference::getAmountWithTax($id) )</td>
                </tr>
                </tfoot>
            </table>
        </div>


    </body>
</html>
