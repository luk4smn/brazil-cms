<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        .header img {
            float: left;
            /*width: 100px;*/
            /*height: 100px;*/
            /*background: #555;*/
        }

        .header h2 {
            position: relative;
            top: 18px;
            /*left: 10px;*/
        }

        .text-small {
            font-size: x-small;
        }
    </style>

    <title>ANÁLISE DE CRÉDITO</title>
</head>
<body>

    <div class="header">
        <img src="{{ public_path('vendor/img/logo/logo_report.png') }}" alt="Logo" height="75px">
        <h2 align="center"><u>ANÁLISE DE CRÉDITO</u></h2>
    </div>
    <br><br>

    <table cellpadding="7" width="100%" border="0">
        <tbody>
            <tr>
                <td valign="top" width="60%">
                    <p class="text-small"><strong>Cliente:</strong> {{ $client->DESCRICAO }}</p>
                    <p class="text-small"><strong>Nome Fantasia:</strong> {{ $client->NOMEFANTASIA }}</p>
                    <p class="text-small"><strong>Endereço:</strong> {{ $client->ENDERECO }}</p>
                    <p class="text-small"><strong>Cidade:</strong> {{ $client->CIDADE }}</p>
                    <p class="text-small"><strong>Data de Cadastro:</strong> {{ $client->DATACADASTRO ? date('d/m/Y H:i:s', strtotime($client->DATACADASTRO)) : null }}</p>
                    <br>
                    <p class="text-small"><strong>Limite de Crédito:</strong> @money($client->LIMITECREDITO)</p>
                </td>
                <td valign="top" width="40%">
                    <p class="text-small"><strong>Data da Consulta:</strong> {{ now()->format('d/m/Y H:i:s') }}</p>
                    <p class="text-small"><strong>Cód. Cliente:</strong> {{ $client->CODIGOPARCEIRO }}</p>
                    <p class="text-small"><strong>Bairro:</strong> {{ $client->BAIRRO }}</p>
                    <p class="text-small"><strong>UF:</strong> {{ $client->ESTADO }}</p>
                    <p class="text-small"><strong>Última Atualização:</strong> {{ date('d/m/Y H:i:s', strtotime($client->ATUALIZACAO)) }}</p>
                    <br>
                    <p class="text-small"><strong>Crédito Disponível:</strong> @money($client->LIMITECREDITO - $client->EMABERTO[0]->VALOREMABERTO)</p>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="table-responsive" style="border: 1px solid #000;">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align:center;" colspan="2">Última Compra nos Últimos 12 meses (Cheque ou Boleto)</th>
            </tr>
            </thead>
            <tbody>
            @if($items['lastMovimentByType'])
            <tr>
                <td valign="top" width="50%">
                    <p class="text-small"><strong>Data:</strong> {{ date('d/m/Y', strtotime($items['lastMovimentByType']->DATADIGITACAO)) }}</p>
                    <p class="text-small"><strong>Forma de Pagamento:</strong> {{ $items['lastMovimentByType']->paymentType }}</p>
                    <p class="text-small"><strong>Total Recebido:</strong> @money(\App\Entities\Reports\CreditAnalysis::valueRecived($items['lastMovimentByType']->releases))</p>
                    <p class="text-small"><strong>Atraso Médio (dias):</strong> {{ \App\Entities\Reports\CreditAnalysis::averageOverdueDays($items['lastMovimentByType']->releases) }}</p>
                </td>
                <td valign="top" width="50%">
                    <p class="text-small"><strong>Pedido:</strong> {{ $items['lastMovimentByType']->NUMEROMOVIMENTO }}</p>
                    <p class="text-small"><strong>Valor:</strong> @money($items['lastMovimentByType']->TOTALLIQUIDO)</p>
                    <p class="text-small"><strong>Quantidade de Parcelas:</strong> {{ count($items['lastMovimentByType']->releasesByType) }}</p>
                    <p class="text-small"><strong>Total à Receber:</strong> @money(\App\Entities\Reports\CreditAnalysis::valueToRecive($items['lastMovimentByType']->releases))</p>
                </td>
            </tr>
            @else
                <tr>
                    <td style="text-align:center;" colspan="2">Não há dados a serem exibidos</td>
                </tr>
            @endif
            </tbody>
            <tfoot align="right">

            </tfoot>
        </table>
    </div>
    <hr>
    <div class="table-responsive" style="border: 1px solid #000;">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align:center;" colspan="2">Maior Compra (Cheque ou Boleto)</th>
            </tr>
            </thead>
            <tbody>
            @if($items['maxMovimentByType'])
            <tr>
                <td valign="top" width="50%">
                    <p class="text-small"><strong>Data:</strong> {{ date('d/m/Y', strtotime($items['maxMovimentByType']->DATADIGITACAO)) }}</p>
                    <p class="text-small"><strong>Forma de Pagamento:</strong> {{ $items['maxMovimentByType']->paymentType }}</p>
                    <p class="text-small"><strong>Total Recebido:</strong> @money(\App\Entities\Reports\CreditAnalysis::valueRecived($items['maxMovimentByType']->releases))</p>
                    <p class="text-small"><strong>Atraso Médio (dias):</strong> {{ \App\Entities\Reports\CreditAnalysis::averageOverdueDays($items['maxMovimentByType']->releases) }}</p>
                </td>
                <td valign="top" width="50%">
                    <p class="text-small"><strong>Pedido:</strong> {{ $items['maxMovimentByType']->NUMEROMOVIMENTO }}</p>
                    <p class="text-small"><strong>Valor:</strong> @money($items['maxMovimentByType']->TOTALLIQUIDO)</p>
                    <p class="text-small"><strong>Quantidade de Parcelas:</strong> {{ count($items['maxMovimentByType']->releasesByType) }}</p>
                    <p class="text-small"><strong>Total à Receber:</strong> @money(\App\Entities\Reports\CreditAnalysis::valueToRecive($items['maxMovimentByType']->releases))</p>
                </td>
            </tr>
            @else
                <tr>
                    <td style="text-align:center;" colspan="2">Não há dados a serem exibidos</td>
                </tr>
            @endif
            </tbody>
            <tfoot align="right">

            </tfoot>
        </table>
    </div>
    <hr>
    <div class="table-responsive" style="border: 1px solid #000;">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align:center;" colspan="8">Contas Recebidas nos últimos 12 meses (Cheque e Boleto)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>#</th>
                <th>Título</th>
                <th>Valor (R$)</th>
                <th>Vencimento</th>
                <th>Dt. Pagamento</th>
                <th>Tipo</th>
                <th>Atraso (Dias)</th>
                <th>Valor Pago</th>
            </tr>
            <?php $totalValue = 0; $i = 1; $totalPaid = 0; $totalOverdue = 0;?>
            @forelse(collect($items['releasesRecived'])->unique('NUMEROLANCAMENTO') as $release)
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $release->NUMEROLANCAMENTO }}</td>
                    <td>@money($release->VALORDOCUMENTO)</td>
                    <td>{{ date('d/m/Y', strtotime($release->DATAVENCIMENTO)) }}</td>
                    <td>{{ date('d/m/Y', strtotime($release->DATAULTIMABAIXA)) }}</td>
                    <td>{{ $release->EVENTO_DESCRICAO }}</td>
                    <td>{{ \App\Entities\Reports\CreditAnalysis::overdueDays($release) }}</td>
                    <td>@money($release->VALORBAIXADO)</td>
                </tr>
                @php
                    $i++;
                    $totalValue += $release->VALORDOCUMENTO;
                    $totalPaid += $release->VALORBAIXADO;
                    $totalOverdue += \App\Entities\Reports\CreditAnalysis::overdueDays($release);
                @endphp
            @empty
                <tr>
                    <td style="text-align:center;" colspan="8">Não há dados a serem exibidos</td>
                </tr>
            @endforelse

            </tbody>
            <tfoot>
            <tr>
                <th style="color: white">.</th>
                <th>Total:</th>
                <th>@money($totalValue)</th>
                <th style="color: white">.</th>
                <th>Atraso Médio:</th>
                <th>{{ round(($totalOverdue / (count($items['releasesRecived']) ? count($items['releasesRecived']) :  1)), 2) }}</th>
                <th>{{ $totalOverdue }}</th>
                <th>@money($totalPaid)</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <hr>
    <div class="table-responsive" style="border: 1px solid #000;">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align:center;" colspan="6">Contas à receber (Cheque e Boleto)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>#</th>
                <th>Título</th>
                <th>Valor (R$)</th>
                <th>Vencimento</th>
                <th>Tipo</th>
                <th>Atraso (Dias)</th>
            </tr>
            <?php $totalValuePending = 0; $j = 1; $totalOverduePending = 0;?>
            @forelse(collect($items['releasesPending'])->unique('NUMEROLANCAMENTO') as $release)
                <tr>
                    <td>{{ $j }}</td>
                    <td>{{ $release->NUMEROLANCAMENTO }}</td>
                    <td>@money($release->VALORDOCUMENTO)</td>
                    <td>{{ date('d/m/Y', strtotime($release->DATAVENCIMENTO)) }}</td>
                    <td>{{ $release->EVENTO_DESCRICAO }}</td>
                    <td>{{ \App\Entities\Reports\CreditAnalysis::overdueDays($release) }}</td>
                </tr>
                @php
                    $j++;
                    $totalValuePending += $release->VALORDOCUMENTO;
                    $totalOverduePending += \App\Entities\Reports\CreditAnalysis::overdueDays($release);
                @endphp
            @empty
                <tr>
                    <td style="text-align:center;" colspan="7">Não há dados a serem exibidos</td>
                </tr>
            @endforelse

            </tbody>
            <tfoot>
            <tr>
                <th style="color: white">.</th>
                <th>Total:</th>
                <th>@money($totalValuePending)</th>
                <th style="color: white">.</th>
                <th style="color: white">.</th>
                <th>{{ $totalOverduePending }}</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <hr>
    <div class="table-responsive" style="border: 1px solid #000;">
        <table class="table table-striped table-sm text-small">
            <thead>
            <tr>
                <th style="text-align:center;" colspan="7">Compras dos últimos 12 meses</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>#</th>
                <th>Pedido</th>
                <th>Valor (R$)</th>
                <th>Data da Compra</th>
                <th>Vendedor</th>
                <th>Tipo</th>
                <th>Parcelas</th>
            </tr>
            <?php $totalValue12Months = 0; $k = 1?>
            @forelse($items['allMovimentsLast12Months'] as $moviment)
                <tr>
                    <td>{{ $k }}</td>
                    <td>{{ $moviment->NUMEROMOVIMENTO }}</td>
                    <td>@money($moviment->TOTALLIQUIDO)</td>
                    <td>{{ date('d/m/Y', strtotime($moviment->DATADIGITACAO)) }}</td>
                    <td style="font-size:smaller">{{ App\Entities\ArgosMoviment::getNomeFuncionario($moviment->CODIGOFUNCIONARIO) }} </td>
                    <td>{{ $moviment->paymentType ?? 'Dinheiro' }}</td>
                    <td>{{ count($moviment->releases) }}</td>
                </tr>
                @php
                    $k++;
                    $totalValue12Months += $moviment->TOTALLIQUIDO;
                @endphp
            @empty
                <tr>
                    <td style="text-align:center;" colspan="7">Não há dados a serem exibidos</td>
                </tr>
            @endforelse

            </tbody>
            <tfoot>
            <tr>
                <th style="color: white">.</th>
                <th>Total:</th>
                <th>@money($totalValue12Months)</th>
                <th style="color: white">.</th>
                <th style="color: white">.</th>
                <th style="color: white">.</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <br><br><br>
    <div align="center">
        <p>_________________________________________________</p>
        <p>AUTORIZADOR</p>
    </div>


</body>
</html>
