<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <style type="text/css">
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }

            header {
                padding: 10px 0;
                margin-bottom: 20px;
                border-bottom: 1px solid #AAAAAA;
            }

            #company {
                text-align: left;
            }

            .text-small {
                font-size: x-small;
            }
        </style>

        <title>AVARIA E FALTA #{{ $id }}</title>
    </head>
    <body>
    <header class="clearfix">
        <div id="company">
            <h5>WL Comércio e importação LTDA</h5>
            <div class="text-small">Rua Presidente João Pessoa, 287 - Centro</div>
            <div class="text-small">CEP: 58400-002, Campina Grande - PB</div>
            <div class="text-small">Tel: (83) 3321-2828</div>
        </div>
    </header>
    <div>
        <p class="text-small" style="margin: 0;">Fornecedor: {{ $provider }}</p>
        <p class="text-small" style="margin: 0;">Transportadora: {{ $shipping_co }}</p>
        <p class="text-small" style="margin: 0;">Data: {{ \Carbon\Carbon::parse($created_at)->format("d/m/Y") }}</p>
    </div>
    @if(\App\Entities\Breakdown::hasMissing($id))
        <h4 align="center">CONTROLE DE FALTAS</h4>
        <br>
        <div class="table-responsive">
            <table class="table table-striped table-sm text-small">
                <thead>
                <tr>
                    <th style="text-align: center">QTD</th>
                    <th style="text-align: center">ID</th>
                    <th style="text-align: center">DESCRIÇÃO</th>
                    <th style="text-align: center">CUSTO</th>
                    <th style="text-align: center">TOTAL</th>
                    <th style="text-align: center">IPI</th>
                    <th style="text-align: center">NF</th>
                    <th style="text-align: center">OBS</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    @if($item['type'] == \App\Entities\BreakdownItem::TYPE_ITEM_MISSING)
                    <tr>
                        <td align="center">{{ $item['quantity'] }}</td>
                        <td align="center">{{ $item['aux_code'] }}</td>
                        <td align="left">{{ $item['description'] }}</td>
                        <td align="right">@money( $item['unit_price'] )</td>
                        <td align="right">@money( $item['amount'] )</td>
                        <td align="center">{{ $item['tax'] }}%</td>
                        <td align="center">{{ $item['invoice'] }}</td>
                        <td align="center">{{ \App\Entities\BreakdownItem::getTypeNameStaticly($item['type']) }}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
                <tfoot align="right">
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2"><strong>TOTAL</strong></td>
                        <td>@money( \App\Entities\Breakdown::getAmountMissing($id) )</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2"><strong>TOTAL C/ IPI</strong></td>
                        <td>@money( \App\Entities\Breakdown::getAmountWithTaxMissing($id) )</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    @endif
    @if(\App\Entities\Breakdown::hasBreakdown($id))
        <br>
        <h4 align="center">CONTROLE DE AVARIAS</h4>
        <br>
        <div class="table-responsive">
            <table class="table table-striped table-sm text-small">
                <thead>
                <tr>
                    <th style="text-align: center">QTD</th>
                    <th style="text-align: center">ID</th>
                    <th style="text-align: center">DESCRIÇÃO</th>
                    <th style="text-align: center">CUSTO</th>
                    <th style="text-align: center">TOTAL</th>
                    <th style="text-align: center">IPI</th>
                    <th style="text-align: center">NF</th>
                    <th style="text-align: center">OBS</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    @if($item['type'] == \App\Entities\BreakdownItem::TYPE_ITEM_BREAKDOWN)
                        <tr>
                            <td align="center">{{ $item['quantity'] }}</td>
                            <td align="center">{{ $item['aux_code'] }}</td>
                            <td align="left">{{ $item['description'] }}</td>
                            <td align="right">@money( $item['unit_price'] )</td>
                            <td align="right">@money( $item['amount'] )</td>
                            <td align="center">{{ $item['tax'] }}%</td>
                            <td align="center">{{ $item['invoice'] }}</td>
                            <td align="center">{{ \App\Entities\BreakdownItem::getTypeNameStaticly($item['type']) }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
                <tfoot align="right">
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2"><strong>TOTAL</strong></td>
                    <td>@money( \App\Entities\Breakdown::getAmountBreakdown($id) )</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2"><strong>TOTAL C/ IPI</strong></td>
                    <td>@money( \App\Entities\Breakdown::getAmountWithTaxBreakDown($id) )</td>
                </tr>
                </tfoot>
            </table>
        </div>
    @endif
    @if(\App\Entities\Breakdown::hasLeftOver($id))
        <br>
        <h4 align="center">CONTROLE DE SOBRAS</h4>
        <br>
        <div class="table-responsive">
            <table class="table table-striped table-sm text-small">
                <thead>
                <tr>
                    <th style="text-align: center">QTD</th>
                    <th style="text-align: center">ID</th>
                    <th style="text-align: center">DESCRIÇÃO</th>
                    <th style="text-align: center">CUSTO</th>
                    <th style="text-align: center">TOTAL</th>
                    <th style="text-align: center">IPI</th>
                    <th style="text-align: center">NF</th>
                    <th style="text-align: center">OBS</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    @if($item['type'] == \App\Entities\BreakdownItem::TYPE_ITEM_LEFTOVER)
                    <tr>
                        <td align="center">{{ $item['quantity'] }}</td>
                        <td align="center">{{ $item['aux_code'] }}</td>
                        <td align="left">{{ $item['description'] }}</td>
                        <td align="right">@money( $item['unit_price'] )</td>
                        <td align="right">@money( $item['amount'] )</td>
                        <td align="center">{{ $item['tax'] }}%</td>
                        <td align="center">{{ $item['invoice'] }}</td>
                        <td align="center">{{ \App\Entities\BreakdownItem::getTypeNameStaticly($item['type']) }}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
                <tfoot align="right">
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2"><strong>TOTAL</strong></td>
                        <td>@money( \App\Entities\Breakdown::getAmountLeftOver($id) )</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2"><strong>TOTAL C/ IPI</strong></td>
                        <td>@money( \App\Entities\Breakdown::getAmountWithTaxLeftOver($id) )</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    @endif

    </body>
</html>
