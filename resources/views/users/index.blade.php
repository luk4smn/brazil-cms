@extends('layouts.app', [
    'namePage' => 'Usuários',
    'class' => 'sidebar-mini',
    'activePage' => 'users',
    'activeNav' => '',
    'searchRoute' => route('users.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Usuários</h2>
            <p class="category">Lista de usuários cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                  <a class="btn btn-primary btn-round text-white pull-right" href="{{ route('users.create') }}">{{ __('+') }}</a>
                <div class="col-12 mt-2">
                  @include('alerts.success')
                  @include('alerts.errors')
                </div>
              </div>
              <div class="card-body">
                <div class="toolbar">
                  <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>
                <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>{{ __('Avatar') }}</th>
                      <th>{{ __('Nome') }}</th>
                      <th>{{ __('Email') }}</th>
                      <th>{{ __('Data de cadastro') }}</th>
                      <th class="disabled-sorting text-center">{{ __('Ações') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                      <tr>
                        <td>
                          <span class="avatar avatar-sm rounded-circle">
                            <img src="{{ $user->avatar ?? asset('assets/img/default-avatar.png') }}" alt="" style="max-width: 50px; border-radius: 100px">
                          </span>
                        </td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{ $user->created_at->format('d/m/Y H:i') }}</td>
                        <td align="center">
                          <a class="btn btn-info btn-icon btn-sm " href="{{ route('users.show', $user) }}">
                            <i class="now-ui-icons users_single-02"></i>
                          </a>
                          <a class="btn btn-success btn-icon btn-sm " href="{{ route('users.edit', $user) }}">
                            <i class="now-ui-icons ui-2_settings-90"></i>
                          </a>
                          @if($user->id!=auth()->user()->id)
                            <button title="Delete" class="btn btn-danger btn-icon btn-sm delete-button delete" href="#"
                              data-action="{{ route('users.destroy', $user->id) }}"
                              data-redirect="{{ route('users.index') }}"
                              >
                              <i class="now-ui-icons ui-1_simple-remove"></i>
                            </button>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $users->appends(request()->all())->links('pagination::bootstrap-4') }}
              </div>
              <!-- end content-->
            </div>
            <!--  end card  -->
          </div>
          <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
