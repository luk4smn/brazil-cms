@extends('layouts.app', [
    'namePage' => 'Dashboard',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'home',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Bem vindo, {{ auth()->user()->name }}.</h2>
            <p class="category">Dashboard</p>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body all-icons">
                        <div class="row">

                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons ui-1_email-85"></i>
                                    <a href="http://gmail.com" target="_blank">
                                        <h5><strong>Gmail</strong></h5>
                                    </a>
                                </div>
                            </div>
                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons shopping_shop"></i>
                                    <a href="https://www.nuvemshop.com.br/login/" target="_blank">
                                        <h5><strong>Nuvemshop</strong></h5>
                                    </a>
                                </div>
                            </div>
                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons tech_tv"></i>
                                    <a href="http://{{ env('CONNECT_SERVER_URL')}}/imanager/" target="_blank">
                                        <h5><strong>iManager</strong></h5>
                                    </a>
                                </div>
                            </div>
                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons shopping_credit-card"></i>
                                    <a href="https://www.gearman.com.br/conciliador/#/dashboard" target="_blank">
                                        <h5><strong>Conciliador</strong></h5>
                                    </a>
                                </div>
                            </div>
                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons business_chart-bar-32"></i>
                                    <a href="{{ route('credit-analysis.index') }}">
                                        <h5><strong>Análise de Crédito</strong></h5>
                                    </a>
                                </div>
                            </div>
                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons users_circle-08"></i>
                                    <a href="https://in4.atlassian.net/servicedesk/customer/user/requests?status=open&reporter=all" target="_blank">
                                        <h5><strong>Inform Service Desk</strong></h5>
                                    </a>
                                </div>
                            </div>
                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons shopping_box"></i>
                                    <a href="http://{{ env('CONNECT_SERVER_URL')}}/inventario/" target="_blank">
                                        <h5><strong>Inventory Manager</strong></h5>
                                    </a>
                                </div>
                            </div>
                            <div class="font-icon-list col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                <div class="font-icon-detail">
                                    <i class="now-ui-icons objects_globe"></i>
                                    <a href="http://{{ env('SERVER_URL')}}/mediawiki" target="_blank">
                                        <h5><strong>Media Wiki</strong></h5>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
