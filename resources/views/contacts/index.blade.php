@extends('layouts.app', [
    'namePage' => 'Contatos',
    'class' => 'sidebar-mini',
    'activePage' => 'contacts',
    'activeNav' => '',
    'searchRoute' => route('contacts.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Contatos</h2>
            <p class="category">Lista de itens cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right openModal"
                           data-toggle="modal"
                           data-target="#formModal"
                           data-action="{{ route('contacts.store') }}"
                           data-method="{{ 'POST' }}"
                           data-title="{{ "Adicionar Novo Contato " }}"
                           data-retrievetype="{{ route('contacts.types') }}"
                        >{{ '+' }}
                        </a>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table" cellspacing="0" width="100%">
                            <thead class="text-primary">
                            <tr>
                                <th><strong>Nome</strong></th>
                                <th><strong>E-mail</strong></th>
                                <th><strong>Telefone</strong></th>
                                <th><strong>Celular</strong></th>
                                <th><strong>Tipo</strong></th>
                                <th class="disabled-sorting text-center"><strong>Ações</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($contacts as $contact)
                                <tr>
                                    <td style="font-size: small">{{ $contact->name }}</td>
                                    <td style="font-size: small">{{ $contact->email }}</td>
                                    <td style="font-size: small">{{ $contact->telephone1 ?? $contact->telephone2 ?? $contact->telephone3 }}</td>
                                    <td style="font-size: small">{{ $contact->cellphone1 ?? $contact->cellphone2 ?? $contact->cellphone3 }}</td>
                                    <td style="font-size: small">
                                        @if($contact->type == \App\Entities\Contact::COLABORADOR)
                                            <span class="alert alert-success">{{ $contact->getTypeName() }}</span>
                                        @elseif($contact->type == \App\Entities\Contact::EMPRESA_GRUPO)
                                            <span class="alert alert-default">{{ $contact->getTypeName() }}</span>
                                        @elseif($contact->type == \App\Entities\Contact::FORNECEDOR)
                                            <span class="alert alert-warning">{{ $contact->getTypeName() }}</span>
                                        @elseif($contact->type == \App\Entities\Contact::PROVEDOR_DE_SERVICO)
                                            <span class="alert alert-info">{{ $contact->getTypeName() }}</span>
                                        @elseif($contact->type == \App\Entities\Contact::REPRESENTANTE)
                                            <span class="alert alert-primary">{{ $contact->getTypeName() }}</span>
                                        @else
                                            <span class="alert alert-danger">{{ $contact->getTypeName() }}</span>
                                        @endif

                                    </td>
                                    <td id="bread-actions" align="center">
                                        <button title="Editar" class="btn btn-primary btn-icon btn-sm openModal"
                                                data-toggle="modal"
                                                data-target="#formModal"
                                                data-action="{{ route('contacts.update', $contact->id) }}"
                                                data-method="{{ 'PUT' }}"
                                                data-title="{{ "Editar Contato: #$contact->id $contact->name " }}"
                                                data-retrieve="{{ route('contacts.edit', $contact->id) }}"
                                                data-retrievetype="{{ route('contacts.types') }}"
                                        >
                                            {!! '&#9997;' !!}
                                        </button>

                                        <button title="Delete" class="btn btn-danger btn-icon btn-sm delete-button delete"
                                                href="#"
                                                data-action="{{ route('contacts.destroy', $contact->id) }}"
                                                data-redirect="{{ route('contacts.index') }}"
                                        >
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                    </td>

                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" align="center">Nenhum dado cadastrado até o momento</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $contacts->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

    <contact></contact>
@endsection
