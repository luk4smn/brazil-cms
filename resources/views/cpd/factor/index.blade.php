@extends('layouts.app', [
    'namePage' => 'Fatores',
    'class' => 'sidebar-mini',
    'activePage' => 'cpd',
    'activeNav' => '',
    'searchRoute' => route('factors.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Fatores de Entrada</h2>
            <p class="category">Lista de itens cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right openModal"
                           data-toggle="modal"
                           data-target="#formModal"
                           data-action="{{ route('factors.store') }}"
                           data-method="{{ 'POST' }}"
                           data-title="{{ "Adicionar novo fator " }}"
                        >{{ '+' }}
                        </a>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table" cellspacing="0" width="100%">
                            <thead class=" text-primary">
                            <tr>
                            <tr>
                                <th><strong>Fornecedor</strong></th>
                                <th><strong>Fator WL</strong></th>
                                <th><strong>Fator Cirne</strong></th>
                                <th><strong>IPI</strong></th>
                                <th><strong>Criado em</strong></th>
                                <th><strong>Atualizado em</strong></th>
                                <th><strong>Ações</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($factors as $factor)
                                <tr>
                                    <td>{{ $factor->provider }}</td>
                                    <td>{{ $factor->factor_wl ?? 'Não Possui' }}</td>
                                    <td>{{ $factor->factor_cirne ?? 'Não Possui' }}</td>
                                    <td>{{ $factor->have_tax ? 'Sim' : 'Não' }}</td>
                                    <td>{{ $factor->created_at->format('d/m/Y') }}</td>
                                    <td>{{ $factor->updated_at->format('d/m/Y') }}</td>

                                    <td id="bread-actions" class="pull-left">
                                        <button title="Editar" class="btn btn-icon btn-sm btn-primary pull-center openModal"
                                                data-toggle="modal"
                                                data-target="#formModal"
                                                data-action="{{ route('factors.update', $factor->id) }}"
                                                data-method="{{ 'PUT' }}"
                                                data-title="{{ "Editar Fator: #$factor->id $factor->provider " }}"
                                                data-retrieve="{{ route('factors.edit', $factor->id) }}"
                                        >
                                            {!! '&#9997;' !!}
                                        </button>

                                        <button title="Delete" class="btn btn-icon btn-sm btn-danger pull-center delete"
                                                href="#"
                                                data-action="{{ route('factors.destroy', $factor->id) }}"
                                                data-redirect="{{ route('factors.index') }}"
                                        >
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                    </td>

                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" align="center">Nenhum dado cadastrado até o momento</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $factors->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

    <factor></factor>
@endsection

