@extends('layouts.app', [
    'namePage' => 'Conferências',
    'class' => 'sidebar-mini',
    'activePage' => 'cpd',
    'activeNav' => '',
    'searchRoute' => route('contacts.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Conferencia Manual</h2>
            <p class="category">Envio de arquivos XML</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="{{ route('recounts.store') }}" method="post" enctype="multipart/form-data" target="_blank">
                            @csrf()
                            <input id="input-xml" name="input-xml[]" type="file" class="file" data-browse-on-zone-click="true" multiple>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
