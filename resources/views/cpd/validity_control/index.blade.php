@extends('layouts.app', [
    'namePage' => 'Validades',
    'class' => 'sidebar-mini',
    'activePage' => 'cpd',
    'activeNav' => '',
    'searchRoute' => route('validities.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Controle de validade</h2>
            <p class="category">Lista de itens cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white" href="{{ route('validities.index') }}">
                            Todos
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('validities.index', ['status' => \App\Entities\Validity::STATUS_RESOLVED]) }}">
                            Resolvidos
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('validities.index', ['status' => \App\Entities\Validity::STATUS_PENDING]) }}">
                            Pendentes
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('validities.index', ['status' => \App\Entities\Validity::STATUS_IN_PROGRESS]) }}">
                            Procedendo
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('validities.index', ['situation' => \App\Entities\Validity::TO_EXPIRE]) }}">
                            A Vencer
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('validities.index', ['situation' => \App\Entities\Validity::EXPIRED]) }}">
                            Vencido
                        </a>

                        <a class="btn btn-primary btn-round text-white pull-right openModal"
                           data-toggle="modal"
                           data-target="#formModal"
                           data-action="{{ route('validities.store') }}"
                           data-method="{{ 'POST' }}"
                           data-title="{{ "Adicionar novo vencimento " }}"
                           data-retrieve-status="{{ route('validities.status') }}"
                           data-find-product="{{ route('product.get', 'ID') }}"
                        >{{ '+' }}
                        </a>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table" cellspacing="0" width="100%">
                            <thead class=" text-primary">
                            <tr>
                            <tr><th><strong>Fornecedor</strong></th>
                                <th><strong>Cod. Aux.</strong></th>
                                <th><strong>Descrição</strong></th>
                                <th><strong>Qtd</strong></th>
                                <th><strong>Lote</strong></th>
                                <th><strong>Validade</strong></th>
                                <th><strong>Preço</strong></th>
                                <th><strong>Situação</strong></th>
                                <th><strong>Status</strong></th>
                                <th><strong>Ações</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($validities as $validity)
                                <tr>
                                    <td style="font-size: x-small">{{ $validity->provider }}</td>
                                    <td style="font-size: x-small">{{ $validity->aux_code }}</td>
                                    <td style="font-size: x-small">{{ $validity->description }}</td>
                                    <td style="font-size: x-small">{{ $validity->quantity }}</td>
                                    <td style="font-size: x-small">{{ $validity->lot }}</td>
                                    <td style="font-size: x-small">{{ $validity->validity_date->format('d/m/Y') }}</td>
                                    <td style="font-size: x-small">@money($validity->price)</td>
                                    <td style="font-size: x-small">
                                        @if($validity->getSituation() == 'Vencido')
                                                <span class="alert alert-danger">{{ $validity->getSituation() }}</span>
                                        @else
                                                <span class="alert alert-info">{{ $validity->getSituation() }}</span>
                                        @endif
                                    </td>
                                    <td style="font-size: x-small">
                                        @if($validity->status == \App\Entities\Validity::STATUS_PENDING)
                                            <span class="alert alert-danger">{{ $validity->getStatusName() }}</span>
                                        @elseif($validity->status ==  \App\Entities\Validity::STATUS_IN_PROGRESS)
                                            <span class="alert alert-warning">{{ $validity->getStatusName() }}</span>
                                        @else
                                            <span class="alert alert-success">{{ $validity->getStatusName() }}</span>
                                        @endif
                                    </td>
                                    <td id="bread-actions" class="pull-left">
                                        <button title="Editar" class="btn btn-icon btn-sm btn-primary pull-center openModal"
                                                data-toggle="modal"
                                                data-target="#formModal"
                                                data-action="{{ route('validities.update', $validity->id) }}"
                                                data-method="{{ 'PUT' }}"
                                                data-retrieve-status="{{ route('validities.status') }}"
                                                data-title="{{ "Editar vencimento: #$validity->id $validity->provider " }}"
                                                data-retrieve="{{ route('validities.edit', $validity->id) }}"
                                                data-find-product="{{ route('product.get', 'ID') }}"
                                        >
                                            {!! '&#9997;' !!}
                                        </button>

                                        <button title="Delete" class="btn btn-icon btn-sm btn-danger pull-center delete"
                                                href="#"
                                                data-action="{{ route('validities.destroy', $validity->id) }}"
                                                data-redirect="{{ route('validities.index') }}"
                                        >
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                    </td>

                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" align="center">Nenhum dado cadastrado até o momento</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $validities->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
<validity-control></validity-control>
@endsection

