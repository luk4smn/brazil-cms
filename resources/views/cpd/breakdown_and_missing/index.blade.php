@extends('layouts.app', [
    'namePage' => 'Avarias',
    'class' => 'sidebar-mini',
    'activePage' => 'cpd',
    'activeNav' => '',
    'searchRoute' => route('breakdowns.index'),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Avaria e Falta</h2>
            <p class="category">Lista de itens cadastrados</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white" href="{{ route('breakdowns.index') }}">
                            Todos
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('breakdowns.index', ['status' => \App\Entities\Breakdown::STATUS_RESOLVED]) }}">
                            Resolvidos
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('breakdowns.index', ['status' => \App\Entities\Breakdown::STATUS_PENDING]) }}">
                            Pendentes
                        </a>
                        <a class="btn btn-primary btn-round text-white" href="{{ route('breakdowns.index', ['status' => \App\Entities\Breakdown::STATUS_IN_PROGRESS]) }}">
                            Procedendo
                        </a>

                        <a class="btn btn-primary btn-round text-white pull-right openModal"
                           data-toggle="modal"
                           data-target="#formModal"
                           data-action="{{ route('breakdowns.store') }}"
                           data-method="{{ 'POST' }}"
                           data-title="{{ "Adicionar nova Avaria / Falta " }}"
                           data-retrieve-types="{{ route('breakdowns.types') }}"
                           data-retrieve-items-types="{{ route('breakdowns.items.types') }}"
                           data-retrieve-status="{{ route('breakdowns.status') }}"
                           data-find-product="{{ route('product.get', 'ID') }}"
                        >{{ '+' }}
                        </a>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table" cellspacing="0" width="100%">
                            <thead class=" text-primary">
                            <tr>
                            <tr>
                                <th><strong>Fornecedor</strong></th>
                                <th><strong>Tipo</strong></th>
                                <th><strong>Usuário</strong></th>
                                <th><strong>Criado em</strong></th>
                                <th><strong>Atualizado em</strong></th>
                                <th><strong>Status</strong></th>
                                <th class="disabled-sorting text-center"><strong>{{ __('Ações') }}</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($breakdowns as $breakdown)
                                <tr>
                                    <td>{{ $breakdown->provider }}</td>
                                    <td>{{ $breakdown->getTypeName() }}</td>
                                    <td style="font-size: small">{{ $breakdown->user->name }}</td>
                                    <td style="font-size: small">{{ $breakdown->created_at->format('d/m/Y') }}</td>
                                    <td style="font-size: small">{{ $breakdown->updated_at->format('d/m/Y') }}</td>
                                    <td style="font-size: x-small">
                                        @if($breakdown->status == \App\Entities\Breakdown::STATUS_PENDING)
                                            <span class="alert alert-danger">{{ $breakdown->getStatusName() }}</span>
                                        @elseif($breakdown->status == \App\Entities\Breakdown::STATUS_IN_PROGRESS)
                                            <span class="alert alert-warning">{{ $breakdown->getStatusName() }}</span>
                                        @else
                                            <span class="alert alert-success">{{ $breakdown->getStatusName() }}</span>
                                        @endif
                                    </td>
                                    <td id="bread-actions" class="pull-left">
                                        <form class="pull-left" action="{{ route('pdf.generate', ['view' => 'breakdown_and_missing']) }}" method="post" target="_blank">
                                            @csrf()
                                            <input id="data" name="data" type="hidden" value="{{ json_encode(array_merge($breakdown->toArray(),['items' => $breakdown->items->toArray()])) }}">

                                            <button title="Gerar PDF" class="btn btn-icon btn-sm btn-warning" type="submit">
                                                {!! '🖨️' !!}
                                            </button>
                                            <span style="color: white; font-size: xx-small" >.</span>
                                        </form>

                                        <button title="Editar" class="btn btn-primary btn-icon btn-sm pull-center openModal"
                                                data-toggle="modal"
                                                data-target="#formModal"
                                                data-action="{{ route('breakdowns.update', $breakdown->id) }}"
                                                data-method="{{ 'PUT' }}"
                                                data-title="{{ "Editar Avaria / Falta: #$breakdown->id $breakdown->provider " }}"
                                                data-retrieve="{{ route('breakdowns.edit', $breakdown->id) }}"
                                                data-retrieve-items="{{ route('breakdowns.items', $breakdown->id) }}"
                                                data-retrieve-types="{{ route('breakdowns.types') }}"
                                                data-retrieve-items-types="{{ route('breakdowns.items.types') }}"
                                                data-retrieve-status="{{ route('breakdowns.status') }}"
                                                data-find-product="{{ route('product.get', 'ID') }}"
                                        >
                                            {!! '&#9997;' !!}
                                        </button>

                                        <button title="Delete" class="btn btn-icon btn-sm btn-danger pull-center delete"
                                                href="#"
                                                data-action="{{ route('breakdowns.destroy', $breakdown->id) }}"
                                                data-redirect="{{ route('breakdowns.index') }}"
                                        >
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" align="center">Nenhum dado cadastrado até o momento</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $breakdowns->appends(request()->all())->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

    <breakdown></breakdown>
@endsection
