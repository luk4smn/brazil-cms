@extends('layouts.app', [
    'namePage' => 'Relatórios',
    'class' => 'sidebar-mini',
    'activePage' => 'reports',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Relatório</h2>
            <p class="category">Análise de crédito</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form action="{{ route('credit-analysis.post') }}" method="post" enctype="multipart/form-data" target="_blank" id="reportForm">
                                    @csrf()
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="text-primary" for="code"><strong>Código do parceiro</strong></label>
                                            <input type="number" name="code" class="form-control" id="code" min="1" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Gerar</button>
                                    </div>

                                   {{-- <button title="Gerar PDF" class="btn btn-primary load_report" data-action="{{ route('credit-analysis.post') }}">
                                        Gerar
                                    </button>--}}
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection



