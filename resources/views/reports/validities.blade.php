@extends('layouts.app', [
    'namePage' => 'Relatórios',
    'class' => 'sidebar-mini',
    'activePage' => 'reports',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Relatório</h2>
            <p class="category">Controle de Validades</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form action="{{ route('validities-report.post') }}" method="post" enctype="multipart/form-data" target="_blank" id="reportForm">
                                    @csrf()

                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="text-primary" for="lot"><strong>Data de Validade (inicio)</strong></label>
                                                    <input type="date" name="validity_date_begin" class="form-control" id="validity_date" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="text-primary" for="lot"><strong>Data de Validade (fim)</strong></label>
                                                    <input type="date" name="validity_date_end" class="form-control" id="validity_date" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <div class="form-group">
                                                <label class="text-primary" for="description"><strong>Status</strong></label>
                                                <select class="selectpicker form-control" name="status" title="Selecionar Status">
                                                    <option disabled selected>Selecione</option>
                                                    @foreach($status as $key => $item)
                                                    <option value="{{ $key }}">{{ $item }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <div class="form-group">
                                                <label class="text-primary" for="description"><strong>Situação</strong></label>
                                                <select class="selectpicker form-control" name="situation" title="Selecionar Situação">
                                                    <option disabled selected>Selecione</option>
                                                    <option value="1">Vencido</option>
                                                    <option value="2">A Vencer</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-xs-4">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="text-primary" for="aux_code"><strong>Código do auxiliar</strong></label>
                                                    <input type="number" name="aux_code" class="form-control" id="aux_code" min="0" placeholder="Código do auxiliar">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <div class="form-group">
                                                <label class="text-primary" for="provider"><strong>Fornecedor</strong></label>
                                                <input type="text" name="provider" class="form-control" id="provider" placeholder="Fornecedor">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <div class="form-group">
                                                <label class="text-primary" for="description"><strong>Descrição</strong></label>
                                                <input type="text" name="description" class="form-control" id="description" placeholder="Descrição">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Gerar</button>
                                    </div>

                                    {{-- <button title="Gerar PDF" class="btn btn-primary load_report" data-action="{{ route('validities-report.post') }}">
                                         Gerar
                                     </button>--}}
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection



