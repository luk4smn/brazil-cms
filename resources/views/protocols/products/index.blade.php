@extends('layouts.app', [
    'namePage' => 'Protocolos',
    'class' => 'sidebar-mini',
    'activePage' => 'protocols',
    'activeNav' => '',
    'searchRoute' => route('protocols.index', ['t' => \App\Entities\Protocol::TYPE_PRODUCT]),
])

@section('content')
    <div class="panel-header panel-header-md">
        <div class="header text-center">
            <h2 class="title">Protocolos de Entrada de Mercadorias</h2>
            <p class="category">Itens cadastrados no sistema</p>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right openModal"
                           data-toggle="modal"
                           data-target="#formModal"
                           data-action="{{ route('protocols.store') }}"
                           data-method="{{ 'POST' }}"
                           data-title="{{ "Adicionar novo protocolo de Entrada" }}"
                           data-retrieve-status="{{ route('protocols.status') }}"
                           data-retrieve-equipments="{{ route('equipments.list') }}"
                        >{{ '+' }}
                        </a>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table" cellspacing="0" width="100%">
                            <thead class=" text-primary">
                            <tr>
                                <th><strong># ID</strong></th>
                                <th><strong>Fornecedores</strong></th>
                                <th><strong>Usuário</strong></th>
                                <th><strong>Nº de itens</strong></th>
                                <th><strong>Criado em</strong></th>
                                <th><strong>Atualizado em</strong></th>
                                <th><strong>Status</strong></th>
{{--                                <th><strong>Observações</strong></th>--}}
                                <th class="disabled-sorting text-center"><strong>{{ __('Ações') }}</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($protocols as $protocol)
                                <tr>
                                    <td style="font-size: small">{{ $protocol->id }}</td>
                                    <td style="font-size: small">{{ $protocol->getItemsData() }}</td>
                                    <td style="font-size: small">{{ $protocol->user->name }}</td>
                                    <td style="font-size: small" align="center">{{ $protocol->items->count() }}</td>
                                    <td style="font-size: small">{{ $protocol->created_at->format('d/m/Y') }}</td>
                                    <td style="font-size: small">{{ $protocol->updated_at->format('d/m/Y') }}</td>
                                    <td style="font-size: x-small">
                                        @if($protocol->status == \App\Entities\Protocol::STATUS_PENDING)
                                            <span class="alert alert-danger">{{ $protocol->getStatusName() }}</span>
                                        @elseif($protocol->status == \App\Entities\Protocol::STATUS_IN_PROGRESS)
                                            <span class="alert alert-warning">{{ $protocol->getStatusName() }}</span>
                                        @else
                                            <span class="alert alert-success">{{ $protocol->getStatusName() }}</span>
                                        @endif
                                    </td>
{{--                                    <td style="font-size: small">{{ $protocol->notes }}</td>--}}
                                    <td id="bread-actions">
                                        <form class="pull-left" action="{{ route('pdf.generate', ['view' => 'protocol_product']) }}" method="post" target="_blank">
                                            @csrf()
                                            <input id="data" name="data" type="hidden" value="{{ json_encode(array_merge($protocol->toArray(),['items' => $protocol->items->toArray()])) }}">

                                            <button title="Gerar PDF" class="btn btn-icon btn-sm btn-warning" type="submit">
                                                {!! '🖨️' !!}
                                            </button>
                                            <span style="color: white; font-size: xx-small" >.</span>
                                        </form>

                                        <a title="Editar" class="btn btn-icon btn-primary btn-sm openModal"
                                           data-toggle="modal"
                                           data-target="#formModal"
                                           data-action="{{ route('protocols.update', $protocol->id) }}"
                                           data-method="{{ 'PUT' }}"
                                           data-title="{{ "Editar Protocolo: #$protocol->id" }}"
                                           data-retrieve="{{ route('protocols.edit', $protocol->id) }}"
                                           data-retrieve-items="{{ route('protocols.items', $protocol->id) }}"
                                           data-retrieve-status="{{ route('protocols.status') }}"
                                        >
                                            {!! '&#9997;' !!}
                                        </a>

                                        <button title="Delete" class="btn btn-icon btn-sm btn-danger delete"
                                                href="#"
                                                data-action="{{ route('protocols.destroy', $protocol->id) }}"
                                                data-redirect="{{ route('protocols.index', ['t' => \App\Entities\Protocol::TYPE_PRODUCT]) }}"
                                        >
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" align="center">Nenhum dado cadastrado até o momento</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $protocols->appends(['t' => \App\Entities\Protocol::TYPE_PRODUCT ])->links('pagination::bootstrap-4') }}
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>

    <protocol-product></protocol-product>
@endsection
