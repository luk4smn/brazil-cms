<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::redirect('/',  url('/home'));

Route::group(['middleware' => ['auth']], function () {
    // HOME ROUTE
    Route::get('/home', 'HomeController@index')->name('home');

    // AUXILIAR ROUTES
    Route::get('/contacts/types', 'ContactController@types')->name('contacts.types');
    Route::get('/breakdowns/types', 'BreakdownController@types')->name('breakdowns.types');
    Route::get('/breakdowns/items/types', 'BreakdownController@itemsTypes')->name('breakdowns.items.types');
    Route::get('/breakdowns/status', 'BreakdownController@status')->name('breakdowns.status');
    Route::get('/breakdowns/{id}/items/', 'BreakdownController@items')->name('breakdowns.items');
    Route::get('/price-differences/{id}/items/', 'PriceDifferenceController@items')->name('price-differences.items');
    Route::get('/price-differences/status', 'PriceDifferenceController@status')->name('price-differences.status');
    Route::get('/protocols/status', 'ProtocolController@status')->name('protocols.status');
    Route::get('/protocols/{id}/items/', 'ProtocolController@items')->name('protocols.items');
    Route::get('/equipments/list/', 'EquipmentController@list')->name('equipments.list');
    Route::get('/validities/status', 'ValidityController@status')->name('validities.status');

	//PROFILE ROUTES
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    // REPORTS ROUTES
    Route::get('/reports/credit-analysis', 'ReportsController@creditAnalysisIndex')->name('credit-analysis.index');
    Route::post('/reports/credit-analysis', 'ReportsController@creditAnalysisPost')->name('credit-analysis.post');
    Route::get('/reports/validities', 'ReportsController@validitiesIndex')->name('validities-report.index');
    Route::post('/reports/validities', 'ReportsController@validitiesPost')->name('validities-report.post');

    //ADMINISTRATIVE ROUTES
    Route::get('/admin/sped/', 'AdministrativeController@sped')->name('admin.sped');
    Route::post('/admin/sped/', 'AdministrativeController@spedPost')->name('admin.spedPost');
    Route::get('/admin/xml/', 'AdministrativeController@xml')->name('admin.xml');
    Route::post('/admin/xml/', 'AdministrativeController@xmlPost')->name('admin.xmlPost');
    Route::post('/admin/xml-database/{id}', 'AdministrativeController@xmlDatabase')->name('admin.xmlDb');
    Route::get('/admin/csv/export', 'AdministrativeController@csvExport')->name('admin.csv_export');
    Route::post('/admin/csv/export', 'AdministrativeController@csvPostExport')->name('admin.csvPost_export');
    Route::get('/admin/csv/import', 'AdministrativeController@csvImport')->name('admin.csv_import');
    Route::post('/admin/csv/import', 'AdministrativeController@csvPostImport')->name('admin.csvPost_import');

    // PDF ROUTES
    Route::post('/pdf/{view}/', 'PDFController@generate')->name('pdf.generate');

    // PRODUCT ROUTES
    Route::get('/products/{cod_aux}', 'ProductController@getProduct')->name('product.get');

    // CRUD ROUTES
    Route::resources([
        'contacts'              => 'ContactController',
        'breakdowns'            => 'BreakdownController',
        'recounts'              => 'RecountController',
        'price-differences'     => 'PriceDifferenceController',
        'protocols'             => 'ProtocolController',
        'equipments'            => 'EquipmentController',
        'factors'               => 'FactorController',
        'validities'            => 'ValidityController',
		'users'      			=> 'UserController',
    ]);
});

