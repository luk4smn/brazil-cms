<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceDifferencesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_differences_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('aux_code');
            $table->string('ref');
            $table->string('description');

            $table->unsignedInteger('quantity');
            $table->float('original_price')->nullable();
            $table->float('recived_price')->nullable();
            $table->integer('tax')->nullable()->default(0);
            $table->float('amount')->nullable();
            $table->float('amount_with_tax')->default(0);

            $table->unsignedInteger('type')->default(1);
            $table->unsignedBigInteger('price_difference_id');

            $table->foreign('price_difference_id')->references('id')->on('price_differences');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_differences_items');
    }
}
