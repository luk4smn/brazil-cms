<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceToBreakdownsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('breakdowns_items', function (Blueprint $table) {
            $table->string('invoice')->nullable()->after('type');
            $table->float('amount_with_tax')->default(0)->after('type');
            $table->integer('tax')->nullable()->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('breakdowns_items', function (Blueprint $table) {
            //
        });
    }
}
