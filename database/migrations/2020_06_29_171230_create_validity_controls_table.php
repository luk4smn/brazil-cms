<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidityControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validity_controls', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description');
            $table->unsignedBigInteger('quantity');
            $table->date('validity_date');
            $table->string('lot');
            $table->unsignedTinyInteger('status');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validity_control');
    }
}
