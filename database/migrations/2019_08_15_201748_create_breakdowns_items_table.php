<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreakdownsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breakdowns_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('aux_code');
            $table->string('description');
            $table->unsignedInteger('quantity');
            $table->float('unit_price')->nullable();
            $table->float('amount')->nullable();
            $table->integer('tax')->nullable();
            $table->unsignedInteger('type')->default(\App\Entities\Breakdown::TYPE_BREAKDOWN);
            $table->unsignedBigInteger('breakdown_id');

            $table->foreign('breakdown_id')->references('id')->on('breakdowns');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breakdowns_items');
    }
}
