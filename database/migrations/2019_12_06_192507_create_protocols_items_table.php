<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProtocolsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('protocols_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('protocol_id');
            //ProtocolEquipment
            $table->unsignedBigInteger('equip_id')->nullable()->index();
            $table->unsignedInteger('quantity')->nullable();
            //ProtocolProduct
            $table->string('shipping_co')->nullable();
            $table->string('provider')->nullable();
            $table->unsignedBigInteger('invoice')->nullable();
            $table->unsignedBigInteger('volumes')->nullable();
            //FK & Timestamps
            $table->foreign('protocol_id')->references('id')->on('protocols');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('protocols_items');
    }
}
