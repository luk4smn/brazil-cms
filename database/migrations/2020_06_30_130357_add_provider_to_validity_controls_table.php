<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProviderToValidityControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('validity_controls', function (Blueprint $table) {
            $table->string('provider')->after('id')->nullable();
            $table->string('notes')->after('status')->nullable();
            $table->smallInteger('status')->unsignedTinyInteger('status')->default(\App\Entities\Validity::STATUS_PENDING)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('validity_controls', function (Blueprint $table) {
            //
        });
    }
}
